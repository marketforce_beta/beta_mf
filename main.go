package main

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	router "gitlab.com/victorrb1015/beta_mf/Router"
)

func main() {
	// Start a new fiber app
	app := fiber.New()
	// Middlewares.
	app.Use(
		// Add CORS to each route.
		cors.New(),
		// Add simple logger.
		logger.New(),
	)

	// Send a string back for GET calls to the endpoint "/"
	app.Get("/", func(c *fiber.Ctx) error {
		err := c.SendString("And Phoenix Fell Down!")
		return err
	})

	app.Get("/api/:database/data", func(c *fiber.Ctx) error {
		database := c.Params("database")
		utils.ConnectClient(database)
		err := c.SendString("database is up!")
		//utils.SendSMSEmail()
		return err
	})

	app.Get("/api/delete_temp", func(c *fiber.Ctx) error {
		os.RemoveAll("Temp")
		os.Mkdir("Temp", 0777)
		return c.SendString("Temp folder deleted!")
	})

	//router.PublicRoutes(app)
	router.PrivateRoutes(app)
	//router.NotFoundRoute(app)

	// Start server (with graceful shutdown).
	utils.StartServer(app)
}
