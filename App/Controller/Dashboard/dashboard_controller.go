package Dashboard

import (
	"database/sql"
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

type Response struct {
	TotalSalesByDay  sql.NullFloat64  `json:"total_sales_by_day"`
	TotalClients     int64            `json:"total_clients"`
	Estimates        int64            `json:"estimates"`
	Orders           int64            `json:"orders"`
	Paids            int64            `json:"paids"`
	EstimatesTable   []ResponseTableA `json:"estimates_table"`
	OrdersTable      []ResponseTableA `json:"orders_table"`
	PaidsTable       []ResponseTableA `json:"paids_table"`
	TableB           []ResponseTableB `json:"daily_sales_table"`
	NumberOfOrders   int64            `json:"number_of_orders"`
	WalkingCustomers int64            `json:"walking_customers"`
	TableC           []ResponseTableC `json:"clients_table"`
	TableD           []ResponseTableD `json:"orders_table_d"`
}

type countResponse struct {
	Count int64 `json:"count"`
	Order int64 `json:"order"`
}

type tableA struct {
	Date     string  `json:"date"`
	Total    float64 `json:"total"`
	StatusId int64   `json:"status_id"`
	OrderId  int64   `json:"order_id"`
}

type ResponseTableA struct {
	Date  string  `json:"date"`
	Total float64 `json:"total"`
}

type Temporal struct {
	Date  string  `json:"date"`
	Total float64 `json:"total"`
	Order int64   `json:"order"`
}

type ResponseTableB struct {
	Day   int64   `json:"day"`
	Total float64 `json:"total"`
}

type ResponseTableC struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Orders    int64  `json:"orders"`
}

type ResponseTableD struct {
	Name      string  `json:"name"`
	FirstName string  `json:"first_name"`
	LastName  string  `json:"last_name"`
	Total     float64 `json:"total"`
}

func GetInfoDashboard(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dash Response
	var TotalOfDay sql.NullFloat64
	var TotalClients int64
	var count []countResponse
	var estimates int64
	var orders int64
	var paids int64
	var table []tableA
	var estimatesTable []ResponseTableA
	var paidsTable []ResponseTableA
	var ordersTable []ResponseTableA
	var estimatesTemporal []Temporal
	var ordersTemporal []Temporal
	var ordersTemporal2 []Temporal
	var paidsTemporal []Temporal
	var tableB []ResponseTableB
	var NumberOfOrders int64
	var WalkingCustomers int64
	var tableC []ResponseTableC
	var tableD []ResponseTableD
	db.Raw("SELECT SUM(A.total) FROM beta_orders as A, beta_order_status_details as B WHERE A.id = B.order_id AND B.status_id = 7 AND B.deleted_at IS NULL AND DAYOFMONTH(B.created_at)=DAYOFMONTH(CURRENT_DATE)").Scan(&TotalOfDay)
	dash.TotalSalesByDay = TotalOfDay
	db.Model(&model.BetaClients{}).Count(&TotalClients)
	dash.TotalClients = TotalClients
	db.Raw("SELECT count(*) as 'count', order_id FROM `beta_order_status_details` WHERE deleted_at is NULL AND MONTH(created_at) = MONTH(CURRENT_DATE) GROUP BY order_id").Scan(&count)
	for _, v := range count {
		if v.Count == 1 {
			estimates++
		} else if v.Count == 7 {
			paids++
		} else {
			orders++
		}
	}
	db.Raw("SELECT DATE(A.created_at) as 'date', A.status_id, A.order_id , B.total FROM beta_order_status_details AS A, beta_orders AS B WHERE A.order_id = B.id AND A.deleted_at IS NULL AND MONTH(A.created_at)= MONTH(NOW()) AND YEAR(A.created_at) = YEAR(NOW())").Scan(&table)
	for _, v := range table {
		if v.StatusId == 1 {
			estimatesTemporal = append(estimatesTemporal, Temporal{Date: v.Date, Total: v.Total, Order: v.OrderId})
		} else if v.StatusId == 7 {
			paidsTemporal = append(paidsTemporal, Temporal{Date: v.Date, Total: v.Total, Order: v.OrderId})
		} else {
			ordersTemporal = append(ordersTemporal, Temporal{Date: v.Date, Total: v.Total, Order: v.OrderId})
		}
	}
	if len(estimatesTemporal) >= 1 {
		estimatesTable = append(estimatesTable, ResponseTableA{Date: estimatesTemporal[0].Date, Total: estimatesTemporal[0].Total})
		for x, v := range estimatesTemporal {
			if x != 0 {
				var found bool
				for i, v2 := range estimatesTable {
					if v.Date == v2.Date {
						estimatesTable[i].Total += v.Total
						found = true
						break
					}
				}
				if !found {
					estimatesTable = append(estimatesTable, ResponseTableA{Date: v.Date, Total: v.Total})
				}
			}
		}
	}
	if len(paidsTemporal) >= 1 {
		paidsTable = append(paidsTable, ResponseTableA{Date: paidsTemporal[0].Date, Total: paidsTemporal[0].Total})
		for x, v := range paidsTemporal {
			if x != 0 {
				var found bool
				for i, v2 := range paidsTable {
					if v.Date == v2.Date {
						paidsTable[i].Total += v.Total
						found = true
						break
					}
				}
				if !found {
					paidsTable = append(paidsTable, ResponseTableA{Date: v.Date, Total: v.Total})
				}
			}
		}
	}
	if len(ordersTemporal) >= 1 {
		ordersTemporal2 = append(ordersTemporal2, Temporal{Date: ordersTemporal[0].Date, Total: ordersTemporal[0].Total, Order: ordersTemporal[0].Order})
		for x, v := range ordersTemporal {
			if x != 0 {
				var found bool
				for _, v2 := range ordersTemporal2 {
					if v.Order == v2.Order {
						found = true
					}
				}
				if !found {
					ordersTemporal2 = append(ordersTemporal2, Temporal{Date: v.Date, Total: v.Total, Order: v.Order})
				}
			}
		}
	}
	if len(ordersTemporal2) >= 1 {
		ordersTable = append(ordersTable, ResponseTableA{Date: ordersTemporal2[0].Date, Total: ordersTemporal2[0].Total})
		for x, v := range ordersTemporal2 {
			if x != 0 {
				var found bool
				for i, v2 := range ordersTable {
					if v.Date == v2.Date {
						ordersTable[i].Total += v.Total
						found = true
					}
				}
				if !found {
					ordersTable = append(ordersTable, ResponseTableA{Date: v.Date, Total: v.Total})
				}
			}
		}
	}

	db.Raw("Select DAY(A.created_at) as 'day', SUM(A.total) as 'total' FROM beta_orders as A WHERE A.deleted_at IS NULL GROUP BY DAY(A.created_at)").Scan(&tableB)
	db.Raw("Select count(*) FROM beta_orders as A WHERE A.deleted_at IS NULL AND MONTH(A.created_at) = MONTH(CURRENT_DATE)").Scan(&NumberOfOrders)
	db.Raw("Select count(*)  as 'walking_customers' FROM beta_orders as A WHERE A.deleted_at IS NULL AND client_id = 1 AND MONTH(A.created_at) = MONTH(CURRENT_DATE)").Scan(&WalkingCustomers)
	db.Raw("Select B.id, B.name, B.first_name, B.last_name, count(A.id) as 'orders' FROM beta_orders as A, beta_clients as B WHERE A.client_id = B.id AND A.deleted_at IS NULL AND MONTH(A.created_at) = MONTH(CURRENT_DATE) GROUP BY B.id LIMIT 5").Scan(&tableC)
	db.Raw("Select B.name, B.first_name, B.last_name, A.total FROM beta_orders as A, beta_clients as B WHERE A.client_id = B.id AND A.deleted_at IS NULL AND MONTH(A.created_at) = MONTH(CURRENT_DATE) LIMIT 5").Scan(&tableD)
	dash.NumberOfOrders = NumberOfOrders
	dash.WalkingCustomers = WalkingCustomers
	dash.TableB = tableB
	dash.TableC = tableC
	dash.TableD = tableD
	dash.Estimates = estimates
	dash.Orders = orders
	dash.Paids = paids
	dash.EstimatesTable = estimatesTable
	dash.OrdersTable = ordersTable
	dash.PaidsTable = paidsTable
	return c.JSON(fiber.Map{"status": "success", "message": "Information obtained", "data": dash})
}
