package SpecialPrice

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
	"time"
)

func GetSpecialPrices(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var products []model.BetaProducts
	db.Find(&products)
	var specialPrices []model.BetaSpecialPrice
	db.Find(&specialPrices)
	var categories []model.BetaCategory
	db.Find(&categories)
	if len(products) == 0 {
		return c.Status(204).JSON(fiber.Map{
			"status":  "error",
			"message": "No products found",
			"data":    nil,
		})
	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Data found",
		"data": fiber.Map{
			"products":      products,
			"category":      categories,
			"special_price": specialPrices,
		},
	})
}

func CreateSpecialPrice(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice model.BetaSpecialPrice
	if err := c.BodyParser(&specialPrice); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    err,
		})
	}
	db.Create(&specialPrice)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price created",
		"data":    specialPrice,
	})
}

func GetSpecialPrice(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice model.BetaSpecialPrice
	db.First(&specialPrice, c.Params("id"))
	if specialPrice.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Special price not found",
			"data":    nil,
		})
	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price found",
		"data":    specialPrice,
	})
}

func UpdateSpecialPrice(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice model.BetaSpecialPrice
	db.First(&specialPrice, c.Params("id"))
	if specialPrice.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Special price not found",
			"data":    nil,
		})
	}
	if err := c.BodyParser(&specialPrice); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    nil,
		})
	}
	db.Save(&specialPrice)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price updated",
		"data":    specialPrice,
	})
}

func DeleteSpecialPrice(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice model.BetaSpecialPrice
	db.First(&specialPrice, c.Params("id"))
	if specialPrice.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Special price not found",
			"data":    nil,
		})
	}
	db.Delete(&specialPrice)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price deleted",
		"data":    nil,
	})
}

func CreateSPT(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice model.BetaSpecialPricingTemp
	var client model.BetaClients
	if err := c.BodyParser(&specialPrice); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    err,
		})
	}
	db.Create(&specialPrice)
	db.Find(&client, specialPrice.ClientID)
	client.Spt = true
	client.DateSptUpdated = time.Now()
	db.Save(&client)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price created",
		"data":    specialPrice,
	})
}

func GetSptByClientID(c *fiber.Ctx) error {
	type Spt struct {
		ID           int
		IDProduct    int
		Name         string
		Price        float64
		SpecialPrice float64
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice []model.BetaSpecialPricingTemp
	db.Where("client_id = ?", c.Params("id")).Find(&specialPrice)
	if len(specialPrice) == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Special price not found",
			"data":    nil,
		})
	}
	var sptemp []Spt
	for _, v := range specialPrice {
		var spt Spt
		var product model.BetaProducts
		db.Find(&product, v.ProductID)
		spt.ID = int(v.ID)
		spt.IDProduct = int(v.ProductID)
		spt.Name = product.Name
		spt.Price = product.Price
		spt.SpecialPrice = v.Price
		sptemp = append(sptemp, spt)
	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price found",
		"data":    sptemp,
	})
}

func DeleteSpt(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var specialPrice model.BetaSpecialPricingTemp
	db.First(&specialPrice, c.Params("id"))
	if specialPrice.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Special price not found",
			"data":    nil,
		})
	}
	db.Delete(&specialPrice)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Special price deleted",
		"data":    nil,
	})
}
