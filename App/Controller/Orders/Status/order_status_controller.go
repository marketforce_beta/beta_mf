package OrderStatus

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
	"time"
)

func GetOrderStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orderStatus []model.BetaOrderStatusDetail
	if err := db.Where("order_id = ?", c.Params("id")).Find(&orderStatus).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status present for order", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Status present for order", "data": orderStatus})
}

func CreateOrderStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orderStatus model.BetaOrderStatusDetail
	if err := c.BodyParser(&orderStatus); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Create(&orderStatus).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating status", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Status created", "data": orderStatus})
}

func UpdateOrderStatus(c *fiber.Ctx) error {
	type updateOrderStatus struct {
		StatusID uint      `valid:"required,lte=10" json:"status_id"`
		OrderID  uint      `valid:"required,lte=10" json:"order_id"`
		Date     time.Time `valid:"required,lte=10" json:"date"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orderStatus model.BetaOrderStatusDetail
	var updateStatus updateOrderStatus
	if err := c.BodyParser(&updateStatus); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Where("order_id = ?", c.Params("id")).Find(&orderStatus).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status present for order", "data": nil})
	}
	if err := db.Model(&orderStatus).Updates(updateStatus).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error updating status", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Status updated", "data": orderStatus})
}

func DeleteOrderStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orderStatus model.BetaOrderStatusDetail
	if err := db.Where("order_id = ?", c.Params("id")).Delete(&orderStatus).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error deleting status", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Status deleted", "data": orderStatus})
}
