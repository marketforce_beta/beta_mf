package Documents

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	"gitlab.com/victorrb1015/beta_mf/App/Utils/PDF"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
	"os"
	"strconv"
)

func GetAllDocuments(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var Documents []Model.BetaOrdersDocuments
	db.Find(&Documents)
	if len(Documents) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Documents present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Documents Found", "data": Documents})
}

func Print(c *fiber.Ctx) error {
	type Company struct {
		FullName     string `db:"full_name" json:"full_name"`
		Address      string `db:"address" json:"address"`
		Abbreviation string `db:"abbreviation" json:"abbreviation"`
		Logo         string `db:"logo" json:"logo" validate:"required"`
		OrdersLogo   string `db:"orders_logo" json:"orders_logo"`
		Country      string `db:"country" json:"country"`
		City         string `db:"city" json:"city"`
		State        string `db:"state" json:"state"`
		ZipCode      string `db:"zip_code" json:"zip_code"`
		EmailFrom    string `db:"email_from" json:"email_from"`
		Phone        string `db:"phone" json:"phone"`
		Database     string `db:"company_db" json:"database"`
		WebSite      string `db:"website" json:"website"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	var orderDetails []Model.BetaOrderDetails
	var orderStatus Model.BetaOrderStatusDetail
	var company Company
	var Client Model.BetaClients
	var pdf string
	var document Model.BetaOrdersDocuments
	db.Where("id = ?", c.Params("id")).First(&order)
	if order.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Order present", "data": nil})
	}
	db.Where("order_id = ?", order.ID).Last(&orderStatus)
	db.Where("order_id = ?", order.ID).Find(&orderDetails)
	db.Where("id = ?", order.ClientID).First(&Client)
	database.ConnectDB()
	alphaDB := database.DB
	alphaDB.Where("company_db = ?", data).First(&company)
	var companyInfo PDF.CompanyInfo
	companyInfo.Logo = "Assets/Companies/assets_Acero.png"
	companyInfo.Name = company.FullName
	companyInfo.Address = company.Address
	companyInfo.Web = company.WebSite
	companyInfo.PhoneNumber = company.Phone
	companyInfo.Email = company.EmailFrom
	var estimateInfo PDF.EstimateInfo
	estimateInfo.EstimateNumber = strconv.Itoa(int(order.OrderNumber))
	estimateInfo.Date = order.OrderDate.Format("02/01/2006")
	estimateInfo.AmountDue = fmt.Sprintf("%.2f", order.BalanceDue)
	estimateInfo.ExpirationDate = order.ExpirationDate.Format("02/01/2006")
	if Client.Name != "" {
		estimateInfo.ClientName = Client.Name
	} else {
		estimateInfo.ClientName = Client.FirstName + " " + Client.LastName
	}
	estimateInfo.ClientAddress = Client.Address
	estimateInfo.ClientPhone = Client.Phone
	estimateInfo.ClientEmail = Client.Email
	estimateInfo.Weight = fmt.Sprintf("%.2f", order.TotalWeight)
	estimateInfo.Freight = fmt.Sprintf("%.2f", order.Freight)
	estimateInfo.Subtotal = fmt.Sprintf("%.2f", order.SubTotal)
	estimateInfo.Tax = fmt.Sprintf("%.2f", order.Tax)
	estimateInfo.TaxValue = fmt.Sprintf("%.2f", order.TaxTotal)
	estimateInfo.Total = fmt.Sprintf("%.2f", order.Total)
	estimateInfo.AmountPaid = fmt.Sprintf("%.2f", order.AmountPaid)
	estimateInfo.Balance = fmt.Sprintf("%.2f", order.BalanceDue)
	var items [][]string
	for i, v := range orderDetails {
		var product Model.BetaProducts
		var item PDF.Items
		var color Model.BetaColors
		db.Find(&product, v.ProductID)
		db.Find(&color, v.ColorID)
		item.Item = product.Name
		item.Description = product.Description
		item.Quantity = strconv.Itoa(int(v.Quantity))
		item.Rate = fmt.Sprintf("%.2f", v.Rate)
		item.Price = fmt.Sprintf("%.2f", v.Price)
		item.Weight = fmt.Sprintf("%.2f", v.Weight)
		item.Color = color.Name
		item.Length = fmt.Sprintf("%.2f", v.Length)
		itemJson := []string{strconv.Itoa(i), item.Item, item.Description, item.Color, item.Quantity, item.Rate, item.Length, item.Price, item.Weight}
		items = append(items, itemJson)
	}
	carpet := "Temp/" + data
	if _, err := os.Stat(carpet); os.IsNotExist(err) {
		err = os.Mkdir(carpet, 0755)
		if err != nil {
			return c.JSON(fiber.Map{"status": "error", "message": "Client not created", "data": err})
		}
	}
	if orderStatus.StatusID == 1 {
		pdf = PDF.CreateEstimate(companyInfo, estimateInfo, items, data, order.Note)
		document.Type = "Estimate"
	} else if (orderStatus.StatusID == 2) || (orderStatus.StatusID == 3) || (orderStatus.StatusID == 4) || (orderStatus.StatusID == 5) {
		pdf = PDF.CreateOrder(companyInfo, estimateInfo, items, data)
		document.Type = "Order"
	} else if (orderStatus.StatusID == 6) || (orderStatus.StatusID == 7) {
		document.Type = "Invoice"
		pdf = PDF.CreateInvoice(companyInfo, estimateInfo, items, data, order.Note)
	}
	msg := utils.UploadFile(pdf, data)
	document.OrderID = order.ID
	document.Document = msg
	db.Create(&document)
	return c.JSON(fiber.Map{"status": "success", "message": "The order was printed", "data": msg})
}

func SendEstimate(c *fiber.Ctx) error {
	type Company struct {
		FullName     string `db:"full_name" json:"full_name"`
		Address      string `db:"address" json:"address"`
		Abbreviation string `db:"abbreviation" json:"abbreviation"`
		Logo         string `db:"logo" json:"logo" validate:"required"`
		OrdersLogo   string `db:"orders_logo" json:"orders_logo"`
		Country      string `db:"country" json:"country"`
		City         string `db:"city" json:"city"`
		State        string `db:"state" json:"state"`
		ZipCode      string `db:"zip_code" json:"zip_code"`
		EmailFrom    string `db:"email_from" json:"email_from"`
		Phone        string `db:"phone" json:"phone"`
		Database     string `db:"company_db" json:"database"`
		WebSite      string `db:"website" json:"website"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	var orderDetails []Model.BetaOrderDetails
	var orderStatus Model.BetaOrderStatusDetail
	var company Company
	var Client Model.BetaClients
	db.Where("id = ?", c.Params("id")).First(&order)
	if order.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Order present", "data": nil})
	}
	db.Where("order_id = ?", order.ID).Last(&orderStatus)
	/*if orderStatus.StatusID != 1 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order is not in estimate status", "data": nil})
	}*/
	db.Where("order_id = ?", order.ID).Find(&orderDetails)
	db.Where("id = ?", order.ClientID).First(&Client)
	database.ConnectDB()
	alphaDB := database.DB
	alphaDB.Where("company_db = ?", data).First(&company)
	var companyInfo PDF.CompanyInfo
	companyInfo.Logo = "Assets/Companies/assets_Acero.png"
	companyInfo.Name = company.FullName
	companyInfo.Address = company.Address
	companyInfo.Web = company.WebSite
	companyInfo.PhoneNumber = company.Phone
	companyInfo.Email = company.EmailFrom
	var estimateInfo PDF.EstimateInfo
	estimateInfo.EstimateNumber = strconv.Itoa(int(order.OrderNumber))
	estimateInfo.Date = order.OrderDate.Format("02/01/2006")
	estimateInfo.AmountDue = fmt.Sprintf("%.2f", order.BalanceDue)
	estimateInfo.ExpirationDate = order.ExpirationDate.Format("02/01/2006")
	if Client.Name != "" {
		estimateInfo.ClientName = Client.Name
	} else {
		estimateInfo.ClientName = Client.FirstName + " " + Client.LastName
	}
	estimateInfo.ClientAddress = Client.Address
	estimateInfo.ClientPhone = Client.Phone
	estimateInfo.ClientEmail = Client.Email
	estimateInfo.Weight = fmt.Sprintf("%.2f", order.TotalWeight)
	estimateInfo.Freight = fmt.Sprintf("%.2f", order.Freight)
	estimateInfo.Subtotal = fmt.Sprintf("%.2f", order.SubTotal)
	estimateInfo.Tax = fmt.Sprintf("%.2f", order.Tax)
	estimateInfo.TaxValue = fmt.Sprintf("%.2f", order.TaxTotal)
	estimateInfo.Total = fmt.Sprintf("%.2f", order.Total)
	estimateInfo.AmountPaid = fmt.Sprintf("%.2f", order.AmountPaid)
	estimateInfo.Balance = fmt.Sprintf("%.2f", order.BalanceDue)
	var items [][]string
	for i, v := range orderDetails {
		var product Model.BetaProducts
		var item PDF.Items
		var color Model.BetaColors
		db.Find(&product, v.ProductID)
		db.Find(&color, v.ColorID)
		item.Item = product.Name
		item.Description = product.Description
		item.Quantity = strconv.Itoa(int(v.Quantity))
		item.Rate = fmt.Sprintf("%.2f", v.Rate)
		item.Price = fmt.Sprintf("%.2f", v.Price)
		item.Weight = fmt.Sprintf("%.2f", v.Weight)
		item.Color = color.Name
		item.Length = fmt.Sprintf("%.2f", v.Length)
		itemJson := []string{strconv.Itoa(i), item.Item, item.Description, item.Color, item.Quantity, item.Rate, item.Length, item.Price, item.Weight}
		items = append(items, itemJson)
	}
	carpet := "Temp/" + data
	if _, err := os.Stat(carpet); os.IsNotExist(err) {
		err = os.Mkdir(carpet, 0755)
		if err != nil {
			return c.JSON(fiber.Map{"status": "error", "message": "Client not created", "data": err})
		}
	}
	pdf := PDF.CreateEstimate(companyInfo, estimateInfo, items, data, order.Note)
	msg := utils.UploadFile(pdf, data)
	var document Model.BetaOrdersDocuments
	document.OrderID = order.ID
	document.Document = msg
	db.Create(&document)
	utils.CreateEstimateEmail(estimateInfo.ClientName, estimateInfo.ClientEmail, pdf, "Your Estimate "+strconv.Itoa(int(order.OrderNumber)))
	return c.JSON(fiber.Map{"status": "success", "message": "The order was sent", "data": msg})
}
