package Orders

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"gorm.io/gorm"
	"log"
	"strconv"
	"strings"
	"time"
)

type orderDetailRequest struct {
	ProductID    uint    `valid:"lte=10" json:"product_id"`
	Quantity     uint    `valid:"lte=10" json:"quantity"`
	ColorID      uint    `valid:"lte=10" json:"color_id"`
	Length       float64 `valid:"lte=10.2" json:"length"`
	Inches       float64 `valid:"lte=10.2" json:"inches"`
	Fraction     float64 `valid:"lte=10" json:"fraction"`
	TotalLength  float64 `valid:"lte=10.2" json:"total_length"`
	Weight       float64 `valid:"lte=10.2" json:"weight"`
	Rate         float64 `valid:"lte=10.2" json:"rate"`
	Price        float64 `valid:"lte=10.2" json:"price"`
	AdditionalID uint    `valid:"lte=10" json:"additional_id"`
}

type orderRequest struct {
	UserID          uint                 `valid:"required,lte=10" json:"user_id"`
	PaymentTermID   uint                 `valid:"required,lte=10" json:"payment_term_id"`
	PaymentID       uint                 `valid:"lte=10" json:"payment_id"`
	ClientID        uint                 `valid:"required,lte=10" json:"client_id"`
	OrderNumber     uint                 `valid:"required,lte=10" json:"order_number"`
	ExpirationDate  string               `valid:"lte=10" json:"expiration_date"`
	TotalWeight     float64              `valid:"required,lte=10.2" json:"total_weight"`
	SubTotal        float64              `valid:"required,lte=10" json:"sub_total"`
	Total           float64              `valid:"required,lte=10.2" json:"total"`
	Freight         float64              `valid:"required,lte=10" json:"freight"`
	Tax             float64              `valid:"required,lte=10" json:"tax"`
	TaxTotal        float64              `valid:"required,lte=10" json:"tax_total"`
	FreightNum      float64              `valid:"required,lte=10" json:"freight_num"`
	AmountPaid      float64              `valid:"required,lte=10.2" json:"amount_paid"`
	Amount          float64              `valid:"required,lte=10.2" json:"amount"`
	BalanceDue      float64              `valid:"required,lte=10.2" json:"balance_due"`
	OrderDate       string               `valid:"required,lte=255" json:"order_date"`
	JobPack         bool                 `valid:"lte=255" json:"jobpack"`
	AddressID       uint                 `valid:"lte=10" json:"address_id"`
	ClientFirstName string               `valid:"lte=255" json:"client_first_name"`
	ClientLastName  string               `valid:"lte=255" json:"client_last_name"`
	TypeSave        string               `valid:"lte=1" json:"type_save" form:"type_save"`
	Note            string               `valid:"lte=255" json:"note"`
	Data            []orderDetailRequest `valid:"required" json:"data"`
}

type Notes struct {
	Name      string    `json:"name"`
	LastName  string    `json:"last_name"`
	Note      string    `json:"note"`
	CreatedAt time.Time `json:"created_at"`
}

type OrderList struct {
	ID          uint                          `json:"id"`
	OrderDate   time.Time                     `json:"order_date"`
	FirstName   string                        `json:"first_name"`
	LastName    string                        `json:"last_name"`
	Client      string                        `json:"client"`
	Status      []Model.BetaOrderStatusDetail `json:"status"`
	Notes       []Notes                       `json:"notes"`
	OrderNumber uint                          `json:"order_number"`
}

type ClientStruct struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type AddressList struct {
	ID      uint   `json:"id"`
	Address string `json:"address"`
}

type OrderDetail struct {
	ID              uint    `json:"id"`
	ProductID       uint    `json:"product_id"`
	Product         string  `json:"product"`
	Quantity        uint    `json:"quantity"`
	ColorID         uint    `json:"color_id"`
	Color           string  `json:"color"`
	Length          float64 `json:"length"`
	Inches          float64 `json:"inches"`
	Fraction        float64 `json:"fraction"`
	TotalLength     float64 `json:"total_length"`
	Weight          float64 `json:"weight"`
	Rate            float64 `json:"rate"`
	Price           float64 `json:"price"`
	Type            string  `json:"type"`
	AdditionalID    uint    `json:"additional_id"`
	Additional      string  `json:"additional"`
	FractionStrings string  `json:"fraction_strings"`
}

type OrderGet struct {
	Order   Model.BetaOrders        `json:"order"`
	Client  Model.BetaClients       `json:"client"`
	Address Model.BetaClientAddress `json:"address"`
	Details []OrderDetail           `json:"details"`
	Status  []DetailStatus          `json:"status"`
}

type DetailStatus struct {
	ID        uint      `json:"id"`
	Status    string    `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	Name      string    `json:"name"`
	LastName  string    `json:"last_name"`
}

type ExportResponse struct {
	OrderNumber uint    `json:"order_number"`
	OrderDate   string  `json:"order_date"`
	Name        string  `json:"name"`
	FirstName   string  `json:"first_name"`
	LastName    string  `json:"last_name"`
	Product     string  `json:"product"`
	Description string  `json:"description"`
	Quantity    uint    `json:"quantity"`
	Rate        float64 `json:"rate"`
	Price       float64 `json:"price"`
}

type ExResponse struct {
	OrderNumber uint    `json:"order_number"`
	OrderDate   string  `json:"order_date"`
	Name        string  `json:"name"`
	Product     string  `json:"product"`
	Description string  `json:"description"`
	Quantity    uint    `json:"quantity"`
	Rate        float64 `json:"rate"`
	Price       float64 `json:"price"`
}

type ScheduleBuilderDB struct {
	OrderId     uint   `json:"order_id"`
	Name        string `json:"name"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Description string `json:"description"`
	Color       string `json:"color"`
	Quantity    uint   `json:"quantity"`
}
type ScheduleBuilderResponse struct {
	OrderId     uint   `json:"order_id"`
	Customer    string `json:"customer"`
	Description string `json:"description"`
	Color       string `json:"color"`
	Quantity    uint   `json:"quantity"`
}

func GetOrders(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var orders []Model.BetaOrders
	db.Find(&orders)
	if len(orders) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	var orderLists []OrderList
	for _, id := range orders {
		var orderList OrderList
		var client Model.BetaClients
		var user Model.User
		orderList.ID = id.ID
		orderList.OrderDate = id.OrderDate
		orderList.OrderNumber = id.OrderNumber
		db.Find(&client, id.ClientID)
		if client.Name != "" {
			orderList.Client = client.Name
		} else {
			orderList.Client = client.FirstName + " " + client.LastName
		}
		db.Find(&user, id.UserID)
		orderList.FirstName = user.Name
		orderList.LastName = user.LastName
		var status []Model.BetaOrderStatusDetail
		db.Raw("SELECT * FROM beta_order_status_details WHERE order_id = ? AND deleted_at IS NULL", id.ID).Scan(&status)
		orderList.Status = status
		var notes []Notes
		db.Raw("SELECT A.note , B.name, B.last_name, A.created_at FROM beta_orders_notes AS A, users as B	WHERE A.user_id = B.id AND A.deleted_at IS NULL AND A.order_id = ?", id.ID).Scan(&notes)
		orderList.Notes = notes
		orderLists = append(orderLists, orderList)
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders Found", "data": orderLists})
}

func GetOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var orderGet OrderGet
	var orderDetail []OrderDetail
	var order Model.BetaOrders
	var notes []Model.BetaOrdersNotes
	var details []Model.BetaOrderDetails
	var invoice Model.BetaOrdersDocuments
	var estimate Model.BetaOrdersDocuments
	var orderPDF Model.BetaOrdersDocuments
	var BillOfLanding Model.BetaOrdersDocuments
	var workOrder Model.BetaOrdersDocuments
	var document []Model.BetaOrdersDocuments
	var client Model.BetaClients
	var clientAddress Model.BetaClientAddress
	var statsus []DetailStatus
	db.Where("id = ?", c.Params("id")).First(&order)
	if order.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order not found", "data": nil})
	}
	db.Find(&notes, "order_id = ?", c.Params("id"))
	db.Find(&details, "order_id = ?", c.Params("id"))
	db.Where("type = ? AND order_id = ?", "Invoice", c.Params("id")).Last(&invoice)
	db.Where("type = ? AND order_id = ?", "Estimate", c.Params("id")).Last(&estimate)
	db.Where("type = ? AND order_id = ?", "Order", c.Params("id")).Last(&orderPDF)
	db.Where("type = ? AND order_id = ?", "Bill of Landing", c.Params("id")).Last(&BillOfLanding)
	db.Where("type = ? AND order_id = ?", "Work Order", c.Params("id")).Last(&workOrder)
	db.Find(&client, order.ClientID)
	if order.AddressID != 0 {
		db.Find(&clientAddress, order.AddressID)
	} else {
		clientAddress.Address = ""
	}
	order.OrderNotes = notes
	document = append(document, invoice, estimate, orderPDF, BillOfLanding, workOrder)
	order.OrdersDocuments = document
	db.Raw("SELECT A.id, A.product_id, B.name as 'product', A.quantity, A.color_id, C.name as 'color', A.length,A.inches, A.fraction, A.weight, A.rate, A.price, B.type, A.additional_id FROM beta_order_details as A, beta_products as B, beta_colors as C WHERE A.product_id = B.id AND A.color_id = C.id AND A.deleted_at IS NULL AND A.order_id = ?", c.Params("id")).Scan(&orderDetail)
	db.Raw("SELECT A.ID, B.name  as 'status', A.created_at, C.name, C.last_name FROM beta_order_status_details as A, beta_statuses as B, users as C WHERE A.status_id = B.id AND A.user_id = C.id AND A.deleted_at is NULL AND A.order_id = ?", order.ID).Scan(&statsus)
	for i, id := range orderDetail {
		if id.AdditionalID != 0 {
			var additional Model.BetaProductsAdditional
			db.Find(&additional, id.AdditionalID)
			orderDetail[i].Additional = additional.Name
			orderDetail[i].Type = "N/A"
		}
		switch id.Fraction {
		case 0.00:
			orderDetail[i].FractionStrings = "N/A"
			break
		case 0.01:
			orderDetail[i].FractionStrings = "1/8 Inch"
			break
		case 0.02:
			orderDetail[i].FractionStrings = "1/4 Inch"
			break
		case 0.03:
			orderDetail[i].FractionStrings = "3/8 Inch"
			break
		case 0.05:
			orderDetail[i].FractionStrings = "1/2 Inch"
			break
		case 0.06:
			orderDetail[i].FractionStrings = "5/8 Inch"
			break
		case 0.07:
			orderDetail[i].FractionStrings = "3/4 Inch"
			break
		case 0.08:
			orderDetail[i].FractionStrings = "7/8 Inch"
			break
		}
	}
	orderGet.Order = order
	orderGet.Client = client
	orderGet.Address = clientAddress
	orderGet.Details = orderDetail
	orderGet.Status = statsus
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Order Found",
		"data":    orderGet})
}

func CreateOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	// var client Model.BetaClients
	var ORequest orderRequest
	var status Model.BetaOrderStatusDetail
	if err := c.BodyParser(&ORequest); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	order.UserID = ORequest.UserID
	order.PaymentTermID = ORequest.PaymentTermID
	order.ClientID = ORequest.ClientID
	order.OrderNumber = ORequest.OrderNumber
	date, _ := time.Parse("2006-01-02", ORequest.ExpirationDate)
	order.ExpirationDate = date
	order.SubTotal = ORequest.SubTotal
	order.Total = ORequest.Total
	order.AmountPaid = ORequest.AmountPaid
	order.Amount = ORequest.Amount
	order.BalanceDue = ORequest.BalanceDue
	date2, _ := time.Parse("2006-01-02", ORequest.OrderDate)
	order.OrderDate = date2
	order.JobPack = ORequest.JobPack
	order.TotalWeight = ORequest.TotalWeight
	order.Freight = ORequest.Freight
	order.FreightNum = ORequest.FreightNum
	order.Tax = ORequest.Tax
	order.TaxTotal = ORequest.TaxTotal
	order.AddressID = ORequest.AddressID
	order.Note = ORequest.Note
	if ORequest.PaymentID != 0 {
		order.PaymentID = ORequest.PaymentID
	} else {
		order.PaymentID = 1
	}
	if err := db.Create(&order).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.First(&order, order.ID)
	var errors []Model.BetaOrderDetails
	var errorsDetail []string
	for _, v := range ORequest.Data {
		var orderDetail Model.BetaOrderDetails
		if v.ColorID == 0 {
			v.ColorID = 1
		}
		orderDetail.OrderID = order.ID
		if v.AdditionalID == 0 {
			orderDetail.ProductID = v.ProductID
			orderDetail.Quantity = v.Quantity
			orderDetail.ColorID = v.ColorID
			orderDetail.Length = v.Length
			orderDetail.Inches = v.Inches
			orderDetail.Fraction = v.Fraction
			orderDetail.TotalLength = v.TotalLength
			orderDetail.Weight = v.Weight
			orderDetail.Rate = v.Rate
			orderDetail.Price = v.Price
		} else {
			var additional Model.BetaProductsAdditional
			orderDetail.AdditionalID = v.AdditionalID
			db.Find(&additional, v.AdditionalID)
			orderDetail.ProductID = additional.ProductID
			orderDetail.Quantity = v.Quantity
			orderDetail.ColorID = 1
			orderDetail.Length = 0
			orderDetail.Weight = 0
			orderDetail.Rate = v.Rate
			orderDetail.Price = v.Price
		}
		time.Sleep(1 * time.Second)
		if err := db.Create(&orderDetail).Error; err != nil {
			errors = append(errors, orderDetail)
			errorsDetail = append(errorsDetail, err.Error())
		}
	}
	if len(errors) > 0 {
		fmt.Println(errors)
		fmt.Println(errorsDetail)
		//return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Body invalid", "data": fiber.Map{"errors": errors, "errorsDetail": errorsDetail, "order": order}})
	}
	status.OrderID = order.ID
	status.StatusID = 1
	status.Date = time.Now()
	status.UserID = order.UserID
	db.Create(&status)
	if order.ID == 0 {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating order", "data": nil})
	}
	utils.PrintDocuments(data, order.ID, ORequest.ClientFirstName, ORequest.ClientLastName)
	switch ORequest.TypeSave {
	case "P":
		var document Model.BetaOrdersDocuments
		db.Where("type = 'Invoice'").Last(&document, "order_id = ?", order.ID)
		return c.JSON(fiber.Map{"status": "success", "message": "Order Created", "data": order, "document": document})
	case "E":
		var document Model.BetaOrdersDocuments
		var client Model.BetaClients
		var fullName string
		db.Where("type = 'Invoice'").Last(&document, "order_id = ?", order.ID)
		result := strings.Split(document.Document, "https://storage.googleapis.com/allamericanmbs/")
		db.Find(&client, order.ClientID)
		if client.Name == "" {
			fullName = client.FirstName + " " + client.LastName
		} else {
			fullName = client.Name
		}
		utils.CreateEstimateEmail(fullName, client.Email, result[1], "Your Invoice")
		return c.JSON(fiber.Map{"status": "success", "message": "Order Created", "data": order, "document": document})
	default:
		var details []Model.BetaOrderDetails
		db.Where("order_id = ?", order.ID).Find(&details)
		return c.JSON(fiber.Map{"status": "success", "message": "Order Created", "data": fiber.Map{"order": order, "details": details}})
	}
}

func UpdateOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	var ORequest orderRequest
	fmt.Println(string(c.Body()))
	if err := c.BodyParser(&ORequest); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	order.PaymentTermID = ORequest.PaymentTermID
	order.PaymentID = ORequest.PaymentID
	order.ClientID = ORequest.ClientID
	date, _ := time.Parse("2006-01-02", ORequest.ExpirationDate)
	order.ExpirationDate = date
	order.SubTotal = ORequest.SubTotal
	order.Total = ORequest.Total
	order.AmountPaid = ORequest.AmountPaid
	order.BalanceDue = ORequest.BalanceDue
	date2, _ := time.Parse("2006-01-02", ORequest.OrderDate)
	order.OrderDate = date2
	if ORequest.JobPack {
		order.JobPack = true
	} else {
		order.JobPack = false
	}
	order.TotalWeight = ORequest.TotalWeight
	order.Freight = ORequest.Freight
	order.Amount = ORequest.Amount
	order.FreightNum = ORequest.FreightNum
	order.Tax = ORequest.Tax
	order.TaxTotal = ORequest.TaxTotal
	order.AddressID = ORequest.AddressID
	order.Note = ORequest.Note
	db.Model(&order).Where("id = ?", c.Params("id")).Updates(order)
	db.Delete(&Model.BetaOrderDetails{}, "order_id = ?", c.Params("id"))
	db.Find(&order, c.Params("id"))
	for _, v := range ORequest.Data {
		var orderDetail Model.BetaOrderDetails
		orderDetail.OrderID = order.ID
		orderDetail.ProductID = v.ProductID
		orderDetail.Quantity = v.Quantity
		orderDetail.ColorID = v.ColorID
		orderDetail.Length = v.Length
		orderDetail.Inches = v.Inches
		orderDetail.Fraction = v.Fraction
		orderDetail.TotalLength = v.TotalLength
		orderDetail.Weight = v.Weight
		orderDetail.Rate = v.Rate
		orderDetail.Price = v.Price
		db.Create(&orderDetail)
	}
	utils.PrintDocuments(data, order.ID, ORequest.ClientFirstName, ORequest.ClientLastName)
	var orderGet OrderGet
	var orderDetail []OrderDetail
	var notes []Model.BetaOrdersNotes
	var details []Model.BetaOrderDetails
	var invoice Model.BetaOrdersDocuments
	var estimate Model.BetaOrdersDocuments
	var orderPDF Model.BetaOrdersDocuments
	var BillOfLanding Model.BetaOrdersDocuments
	var workOrder Model.BetaOrdersDocuments
	var document []Model.BetaOrdersDocuments
	var client Model.BetaClients
	var clientAddress Model.BetaClientAddress
	var statsus []DetailStatus
	db.Where("id = ?", c.Params("id")).First(&order)
	if order.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order not found", "data": nil})
	}
	db.Find(&notes, "order_id = ?", c.Params("id"))
	db.Find(&details, "order_id = ?", c.Params("id"))
	db.Where("type = ? AND order_id = ?", "Invoice", c.Params("id")).Last(&invoice)
	db.Where("type = ? AND order_id = ?", "Estimate", c.Params("id")).Last(&estimate)
	db.Where("type = ? AND order_id = ?", "Order", c.Params("id")).Last(&orderPDF)
	db.Where("type = ? AND order_id = ?", "Bill of Landing", c.Params("id")).Last(&BillOfLanding)
	db.Where("type = ? AND order_id = ?", "Work Order", c.Params("id")).Last(&workOrder)
	db.Find(&client, order.ClientID)
	if order.AddressID != 0 {
		db.Find(&clientAddress, order.AddressID)
	} else {
		clientAddress.Address = ""
	}
	order.OrderNotes = notes
	document = append(document, invoice, estimate, orderPDF, BillOfLanding, workOrder)
	order.OrdersDocuments = document
	db.Raw("SELECT A.id, A.product_id, B.name as 'product', A.quantity, A.color_id, C.name as 'color', A.length, A.weight, A.rate, A.price, B.type, A.additional_id FROM beta_order_details as A, beta_products as B, beta_colors as C WHERE A.product_id = B.id AND A.color_id = C.id AND A.deleted_at IS NULL AND A.order_id = ?", order.ID).Scan(&orderDetail)
	db.Raw("SELECT A.ID, B.name  as 'status', A.created_at, C.name, C.last_name FROM beta_order_status_details as A, beta_statuses as B, users as C WHERE A.status_id = B.id AND A.user_id = C.id AND A.deleted_at is NULL AND A.order_id = ?", order.ID).Scan(&statsus)
	for i, id := range orderDetail {
		if id.AdditionalID != 0 {
			var additional Model.BetaProductsAdditional
			db.Find(&additional, id.AdditionalID)
			orderDetail[i].Additional = additional.Name
			orderDetail[i].Type = "N/A"
		}
	}
	orderGet.Order = order
	orderGet.Client = client
	orderGet.Address = clientAddress
	orderGet.Details = orderDetail
	orderGet.Status = statsus
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Order Updated",

		"data": orderGet})
}

func DeleteOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	db.Where("id = ?", c.Params("id")).Delete(&order)
	if order.ID == 0 {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting order", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order Deleted", "data": nil})
}

func GetProductsByClient(c *fiber.Ctx) error {
	type Items struct {
		Id           uint
		Name         string                       `json:"name"`
		Gauge        int                          `json:"gauge"`
		Type         string                       `json:"type"`
		Price        float64                      `json:"price"`
		CatLength    bool                         `json:"cat_length"`
		Colors       bool                         `json:"colors"`
		Length       bool                         `json:"length"`
		Additional   bool                         `json:"additional"`
		CategoryID   int                          `json:"category_id"`
		Description  string                       `json:"description"`
		Weight       float64                      `json:"weight"`
		WeightString string                       `json:"weight_string"`
		Sp           []Model.BetaSpecialPrice     `json:"sp"`
		Spt          Model.BetaSpecialPricingTemp `json:"spt"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var client Model.BetaClients
	var products []Model.BetaProducts
	var item []Items
	if c.Params("id") == "0" {
		db.Find(&products)
		for _, product := range products {
			var sp []Model.BetaSpecialPrice
			var spt Model.BetaSpecialPricingTemp
			if err := db.Where("product_id = ?", product.ID).Find(&sp).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				sp = nil
			}
			if err := db.Where("product_id = ?", product.ID).Where("client_id = ?", client.ID).Find(&spt).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				spt = Model.BetaSpecialPricingTemp{}
			}
			var additional []Model.BetaProductsAdditional
			var bo bool
			if err := db.Find(&additional).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				bo = false
			} else {
				bo = true
			}
			item = append(item, Items{
				Id:           product.ID,
				Name:         product.Name,
				Gauge:        product.Gauge,
				Type:         product.Type,
				Price:        product.Price,
				CatLength:    product.CatLength,
				Colors:       product.Colors,
				Length:       product.Length,
				Description:  product.Description,
				Weight:       product.Weight,
				WeightString: product.WeightString,
				CategoryID:   int(product.ProductsCategoryID),
				Additional:   bo,
				Sp:           sp,
				Spt:          spt,
			})
		}
	} else {
		db.Find(&client, c.Params("id"))
		if client.ID == 0 {
			return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Client not found", "data": nil})
		}
		if client.CategoryID == 1 && client.Spt == false {
			db.Find(&products)
			var additional []Model.BetaProductsAdditional
			var bo bool
			if err := db.Find(&additional).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				bo = false
			} else {
				bo = true
			}
			for _, product := range products {
				item = append(item, Items{
					Id:           product.ID,
					Name:         product.Name,
					Gauge:        product.Gauge,
					Type:         product.Type,
					Price:        product.Price,
					CatLength:    product.CatLength,
					Colors:       product.Colors,
					Length:       product.Length,
					Description:  product.Description,
					Weight:       product.Weight,
					WeightString: product.WeightString,
					CategoryID:   int(product.ProductsCategoryID),
					Additional:   bo,
					Sp:           nil,
					Spt:          Model.BetaSpecialPricingTemp{},
				})
			}
		} else if client.CategoryID != 1 && client.Spt == false {
			db.Find(&products)
			for _, product := range products {
				var sp []Model.BetaSpecialPrice
				if err := db.Where("product_id = ?", product.ID).Find(&sp).Error; errors.Is(err, gorm.ErrRecordNotFound) {
					sp = nil
				}
				var additional []Model.BetaProductsAdditional
				var bo bool
				if err := db.Find(&additional).Error; errors.Is(err, gorm.ErrRecordNotFound) {
					bo = false
				} else {
					bo = true
				}
				item = append(item, Items{
					Id:           product.ID,
					Name:         product.Name,
					Gauge:        product.Gauge,
					Type:         product.Type,
					Price:        product.Price,
					CatLength:    product.CatLength,
					Colors:       product.Colors,
					Length:       product.Length,
					Description:  product.Description,
					Weight:       product.Weight,
					WeightString: product.WeightString,
					CategoryID:   int(product.ProductsCategoryID),
					Additional:   bo,
					Sp:           sp,
					Spt:          Model.BetaSpecialPricingTemp{},
				})
			}
		} else {
			db.Find(&products)
			for _, product := range products {
				var sp []Model.BetaSpecialPrice
				var spt Model.BetaSpecialPricingTemp
				if err := db.Where("product_id = ?", product.ID).Find(&sp).Error; errors.Is(err, gorm.ErrRecordNotFound) {
					sp = nil
				}
				if err := db.Where("product_id = ?", product.ID).Where("client_id = ?", client.ID).Find(&spt).Error; errors.Is(err, gorm.ErrRecordNotFound) {
					spt = Model.BetaSpecialPricingTemp{}
				}
				var additional []Model.BetaProductsAdditional
				var bo bool
				if err := db.Find(&additional).Error; errors.Is(err, gorm.ErrRecordNotFound) {
					bo = false
				} else {
					bo = true
				}
				item = append(item, Items{
					Id:           product.ID,
					Name:         product.Name,
					Gauge:        product.Gauge,
					Type:         product.Type,
					Price:        product.Price,
					CatLength:    product.CatLength,
					Colors:       product.Colors,
					Length:       product.Length,
					Description:  product.Description,
					Weight:       product.Weight,
					WeightString: product.WeightString,
					CategoryID:   int(product.ProductsCategoryID),
					Additional:   bo,
					Sp:           sp,
					Spt:          spt,
				})
			}
		}
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Products Found", "data": item})
}

func GetCatalogLeg(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var catalog []Model.BetaCatalogLength
	db.Where("product_id = ?", c.Params("id")).Find(&catalog)
	return c.JSON(fiber.Map{"status": "success", "message": "Catalog Found", "data": catalog})
}

func GetAdditionalProduct(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var additional []Model.BetaProductsAdditional
	db.Where("product_id = ?", c.Params("id")).Find(&additional)
	return c.JSON(fiber.Map{"status": "success", "message": "Additional Found", "data": additional})
}

func GetNewInvoiceNumber(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var result int = 0
	row := db.Table("beta_orders").Select("MAX(order_number) as invoice_number").Row()
	_ = row.Scan(&result)
	result++
	return c.JSON(fiber.Map{"status": "success", "message": "Order number found", "order": result})
}

func GetPaymentTerms(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var payments []Model.BetaPaymentTerms
	db.Find(&payments)
	if len(payments) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No payments present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Payments Found", "data": payments})
}

func UpdateOrderStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	var orderStatus Model.BetaOrderStatusDetail
	var newStatus Model.BetaOrderStatusDetail
	var intStatus int
	var maxStatus Model.BetaStatus
	if err := db.Find(&order, c.Params("id")).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order not found", "data": nil})
	}
	if err := db.Where("order_id = ?", order.ID).Last(&orderStatus).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order status not found", "data": nil})
	}
	intStatus = int(orderStatus.StatusID) + 1
	if err := db.Find(&maxStatus, intStatus).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Status cannot be updated anymore", "data": nil})
	}
	userId := c.Params("user_id")
	userIdInt, _ := strconv.Atoi(userId)
	newStatus.OrderID = order.ID
	newStatus.StatusID = maxStatus.ID
	newStatus.UserID = uint(userIdInt)
	newStatus.Date = time.Now()
	if err := db.Create(&newStatus).Error; err != nil {

		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Error creating status", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order status updated", "data": newStatus})
}

func DeleteStatusDetail(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orderStatus Model.BetaOrderStatusDetail
	if err := db.Where("id = ?", c.Params("id")).Last(&orderStatus).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order status not found", "data": nil})
	}
	if err := db.Delete(&orderStatus).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting status", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order status deleted", "data": nil})
}

func GetAddressByClientList(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var client Model.BetaClients
	var addressClient []Model.BetaClientAddress
	var addressList []AddressList
	if err := db.Find(&client, c.Params("id")).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Client not found", "data": nil})
	}
	var addresses0 AddressList
	var state Model.BetaClientAddressStates
	db.Where("id = ?", client.StateID).First(&state)
	addresses0.ID = 0
	addresses0.Address = client.Address + " " + client.City + ", " + state.Name + " " + client.Zip
	addressList = append(addressList, addresses0)
	db.Where("client_id = ?", client.ID).Find(&addressClient)
	for _, address := range addressClient {
		var addresses AddressList
		var state Model.BetaClientAddressStates
		db.Where("id = ?", address.StateID).First(&state)
		addresses.ID = address.ID
		addresses.Address = address.Address + " " + address.City + ", " + state.Name + " " + address.Zip
		addressList = append(addressList, addresses)
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Address found", "data": addressList})
}

func SendInvoice(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	var client Model.BetaClients
	var fullName string
	var document Model.BetaOrdersDocuments
	var status []Model.BetaOrderStatusDetail
	var found bool
	db.Find(&order, c.Params("id"))
	if order.ID == 0 {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "No Order present", "data": nil})
	}
	utils.PrintDocuments(data, order.ID, "", "")
	db.Find(&client, order.ClientID)
	if client.Name == "" {
		fullName = client.FirstName + " " + client.LastName
	} else {
		fullName = client.Name
	}
	db.Find(&status, "order_id = ?", order.ID)
	for _, stat := range status {
		if stat.StatusID != 1 {
			found = true
			break
		}
	}
	if !found {
		db.Where("type = 'Invoice'").Last(&document, "order_id = ?", order.ID)
		result := strings.Split(document.Document, "https://storage.googleapis.com/allamericanmbs/")
		utils.CreateEstimateEmail(fullName, client.Email, result[1], "Your Invoice")
	} else {
		db.Where("type = 'Order'").Last(&document, "order_id = ?", order.ID)
		result := strings.Split(document.Document, "https://storage.googleapis.com/allamericanmbs/")
		utils.CreateOrderEmail(fullName, client.Email, result[1], "Your Invoice")
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order Created", "data": order, "document": document})
}

func UpdateDateOrder(c *fiber.Ctx) error {
	type DateOrder struct {
		Date string `json:"date"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	db.Find(&order, c.Params("id"))
	if order.ID == 0 {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "No Order present", "data": nil})
	}
	var dateOrder DateOrder
	if err := c.BodyParser(&dateOrder); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": nil})
	}
	date, _ := time.Parse("2006-01-02", dateOrder.Date)
	order.OrderDate = date
	if err := db.Save(&order).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error updating order", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order updated", "data": order})
}

func ExportOrders(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var request []ExportResponse
	var response []ExResponse
	var orders []uint
	db.Raw("SELECT B.order_number, B.order_date, C.name, C.first_name, C.last_name, D.name as 'product', D.description, A.quantity, A.rate, A.price FROM beta_order_details as A, beta_orders as B, beta_clients as C, beta_products as D WHERE A.order_id = B.id AND B.client_id = C.id AND A.product_id = D.id AND B.export IS FALSE").Scan(&request)
	if len(request) == 0 {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "No Orders present", "data": nil})
	}
	orders = append(orders, request[0].OrderNumber)
	for _, order := range request {
		var exResponse ExResponse
		exResponse.OrderNumber = order.OrderNumber
		exResponse.OrderDate = order.OrderDate
		if order.Name == "" {
			exResponse.Name = order.FirstName + " " + order.LastName
		} else {
			exResponse.Name = order.Name
		}
		exResponse.Product = order.Product
		exResponse.Description = order.Description
		exResponse.Quantity = order.Quantity
		exResponse.Rate = order.Rate
		exResponse.Price = order.Price
		response = append(response, exResponse)
		orders = append(orders, order.OrderNumber)
	}
	var listOrders []uint
	listOrders = removeDuplicateValues(orders)
	for _, order := range listOrders {
		var invoice Model.BetaOrders
		db.Where("order_number = ?", order).First(&invoice)
		invoice.Export = true
		db.Save(&invoice)
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders exported", "data": response})
}

func removeDuplicateValues(intSlice []uint) []uint {
	keys := make(map[uint]bool)
	list := []uint{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func ScheduleBuilder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dataDB []ScheduleBuilderDB
	db.Raw("SELECT A.order_id, C.name, C.first_name, C.last_name, D.description, E.name as 'color', A.quantity FROM beta_order_details as A, beta_orders as B, beta_clients as C, beta_products as D, beta_colors as E WHERE A.order_id = B.id AND B.client_id = C.id AND A.product_id = D.id AND A.color_id = E.id AND B.deleted_at IS NULL AND B.manufactured IS FALSE").Scan(&dataDB)
	if len(dataDB) == 0 {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "No Orders present", "data": nil})
	}
	var response []ScheduleBuilderResponse
	for _, order := range dataDB {
		var res ScheduleBuilderResponse
		if order.Name == "" {
			res.Customer = order.FirstName + " " + order.LastName
		} else {
			res.Customer = order.Name
		}
		res.OrderId = order.OrderId
		res.Description = order.Description
		res.Quantity = order.Quantity
		res.Color = order.Color
		response = append(response, res)
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders exported", "data": response})
}

func SecondaryColors(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var dataDB []Model.BetaSecondaryColors
	db.Find(&dataDB)
	if len(dataDB) == 0 {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "No Colors present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Colors present", "data": dataDB})
}

func InfoCreateOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var OrderNumber int = 0
	var states []Model.BetaClientStates
	var payments []Model.BetaPayment
	var categories []Model.BetaCategory
	var paymentsTerms []Model.BetaPaymentTerms
	var clients []Model.BetaClients
	var colors []Model.BetaColors
	var dataDB []Model.BetaSecondaryColors
	row := db.Table("beta_orders").Select("MAX(order_number) as invoice_number").Row()
	_ = row.Scan(&OrderNumber)
	OrderNumber++
	if err := db.Find(&states).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "No States present", "data": err})
	}
	if len(states) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No states present", "data": nil})
	}
	db.Find(&payments)
	if len(payments) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No payments present", "data": nil})
	}
	db.Find(&categories)
	if len(categories) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No categories present", "data": nil})
	}
	db.Find(&paymentsTerms)
	if len(paymentsTerms) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No payments present", "data": nil})
	}
	db.Find(&clients)
	if len(clients) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No clients present", "data": nil})
	}
	db.Find(&colors)
	if len(colors) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No colors present", "data": nil})
	}
	db.Find(&dataDB)
	if len(dataDB) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Colors present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Info present", "data": fiber.Map{"order_number": OrderNumber, "states": states, "payments": payments, "categories": categories, "payments_terms": paymentsTerms, "clients": clients, "colors": colors, "secondary_colors": dataDB}})
}
