package Notes

import (
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
)

type NotesList struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	LastName  string    `json:"last_name"`
	Note      string    `json:"note"`
	CreatedAt time.Time `json:"created_at"`
}

func CreateOrderNotes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.BetaOrdersNotes
	if err := c.BodyParser(&note); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": err})
	}
	if err := db.Create(&note).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating note", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note Created", "data": note})
}

func GetAllOrderNotes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	id := c.Params("idOrder")
	var noteList []NotesList
	//var notes []model.BetaOrdersNotes

	if err := db.Raw("SELECT A.id, A.note , B.name, B.last_name, A.created_at FROM beta_orders_notes AS A, users as B WHERE A.user_id = B.id AND A.deleted_at IS NULL AND A.order_id = ?", id).Scan(&noteList).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No notes present for order", "data": nil})
	}
	/*if err := db.Where("order_id = ?", c.Params("idOrder")).Find(&notes).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No notes present for order", "data": nil})
	}*/
	return c.JSON(fiber.Map{"status": "success", "message": "Notes Found", "data": noteList})
}

func UpdateOrderNotes(c *fiber.Ctx) error {
	type updateNote struct {
		OrderID uint   `valid:"required,lte=10" json:"order_id"`
		Note    string `valid:"required,lte=255" json:"note"`
		Title   string `valid:"required,lte=255" json:"title"`
		UserID  uint   `valid:"required,lte=10" json:"user_id"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.BetaOrdersNotes
	var update updateNote
	if err := c.BodyParser(&update); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": nil})
	}
	if err := db.Where("order_id = ?", c.Params("id")).Find(&note).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status present for order", "data": nil})
	}
	if err := db.Model(&note).Updates(update).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error updating note", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note Updated", "data": note})
}

func DeleteOrderNotes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.BetaOrdersNotes
	if err := db.Where("id = ?", c.Params("id")).Find(&note).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status present for order", "data": nil})
	}
	if err := db.Delete(&note).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error deleting note", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note Deleted", "data": nil})
}
