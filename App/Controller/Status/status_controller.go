package Status

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

func GetStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var status []model.BetaStatus
	db.Find(&status)
	if len(status) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No colors present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Notes Found", "data": status})
}

func CreateStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var status model.BetaStatus
	if err := c.BodyParser(&status); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Create(&status)
	return c.JSON(fiber.Map{"status": "success", "message": "Status Created", "data": status})
}

func GetStatusById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var status model.BetaStatus
	db.First(&status, c.Params("id"))
	if status.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Status Found", "data": status})
}

func UpdateStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var status model.BetaStatus
	db.First(&status, c.Params("id"))
	if status.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status found", "data": nil})
	}
	if err := c.BodyParser(&status); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Save(&status)
	return c.JSON(fiber.Map{"status": "success", "message": "Status Updated", "data": status})
}

func DeleteStatus(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var status model.BetaStatus
	db.First(&status, c.Params("id"))
	if status.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No status found", "data": nil})
	}
	db.Delete(&status)
	return c.JSON(fiber.Map{"status": "success", "message": "Status Deleted", "data": status})
}
