package Employee

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

func GetAllEmployee(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var employees []model.BetaClientEmployee
	db.Find(&employees)
	if len(employees) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No employees present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Employees found", "data": employees})
}

func GetEmployee(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var employee model.BetaClientEmployee
	db.Where("id = ?", c.Params("id")).First(&employee)
	if employee.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Employee not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Employee found", "data": employee})
}

func CreateEmployee(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var employee model.BetaClientEmployee
	if err := c.BodyParser(&employee); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Create(&employee)
	if employee.ID == 0 {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Employee not created", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Employee created", "data": employee})
}

func UpdateEmployee(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var employee model.BetaClientEmployee
	db.Find(&employee, c.Params("id"))
	if employee.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Employee not found", "data": nil})
	}
	if err := c.BodyParser(&employee); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Save(&employee)
	return c.JSON(fiber.Map{"status": "success", "message": "Employee updated", "data": employee})
}

func DeleteEmployee(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var employee model.BetaClientEmployee
	db.Find(&employee, c.Params("id"))
	if employee.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Employee not found", "data": nil})
	}
	db.Delete(&employee)
	return c.JSON(fiber.Map{"status": "success", "message": "Employee deleted", "data": nil})
}
