package ClientCategory

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
)

func GetClientCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientCategory []model.BetaCategory
	if err := db.Where("client_id = ?", c.Params("id")).Find(&clientCategory).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Categories not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Categories found", "data": clientCategory})
}

func CreateClientCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientCategory model.BetaCategory
	if err := c.BodyParser(&clientCategory); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Create(&clientCategory).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating category", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Category created", "data": clientCategory})
}

func DeleteClientCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientCategory model.BetaCategory
	if err := db.Where("client_id = ? AND id = ?", c.Params("id"), c.Params("category_id")).First(&clientCategory).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Category not found", "data": nil})
	}
	if err := db.Delete(&clientCategory).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error deleting category", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Category deleted", "data": nil})
}
