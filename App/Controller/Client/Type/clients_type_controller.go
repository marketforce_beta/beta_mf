package Type

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

func GetAllClientsType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientsType []model.BetaClientTypes
	db.Find(&clientsType)
	if len(clientsType) == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "No clients type found",
			"data":    nil,
		})
	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Clients type found",
		"data":    clientsType,
	})
}

func CreateClientsType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientsType model.BetaClientTypes
	if err := c.BodyParser(&clientsType); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    nil,
		})
	}
	db.Create(&clientsType)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Client type created",
		"data":    clientsType,
	})
}

func GetClientsType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientsType model.BetaClientTypes
	db.First(&clientsType, c.Params("id"))
	if clientsType.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "No clients type found",
			"data":    nil,
		})
	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Clients type found",
		"data":    clientsType,
	})
}

func UpdateClientsType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientsType model.BetaClientTypes
	db.First(&clientsType, c.Params("id"))
	if clientsType.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "No clients type found",
			"data":    nil,
		})
	}
	if err := c.BodyParser(&clientsType); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    nil,
		})
	}
	db.Save(&clientsType)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Clients type updated",
		"data":    clientsType,
	})
}

func DeleteClientsType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var clientsType model.BetaClientTypes
	db.First(&clientsType, c.Params("id"))
	if clientsType.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "No clients type found",
			"data":    nil,
		})
	}
	db.Delete(&clientsType)
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Clients type deleted",
		"data":    nil,
	})
}
