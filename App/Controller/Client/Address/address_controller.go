package Address

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

func GetAllAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address []model.BetaClientAddress
	db.Find(&address)
	if len(address) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "Address no present", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Address present", "data": address})
}

func CreateAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.BetaClientAddress
	if err := c.BodyParser(&address); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Create(&address)
	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "Address created", "data": address})
}

func GetAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.BetaClientAddress
	db.First(&address, c.Params("id"))
	if address.ID == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No Address present", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Address present", "data": address})
}

func UpdateAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.BetaClientAddress
	db.First(&address, c.Params("id"))
	if address.ID == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No Address present", "data": nil})
	}
	if err := c.BodyParser(&address); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Save(&address)
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Address updated", "data": address})
}

func DeleteAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.BetaClientAddress
	db.First(&address, c.Params("id"))
	if address.ID == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No Address present", "data": nil})
	}
	db.Delete(&address)
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Address deleted", "data": nil})
}

func GetAddressByClient(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address []model.BetaClientAddress
	db.Where("client_id = ?", c.Params("id")).Find(&address)
	if len(address) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No Address present", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Address present", "data": address})
}
