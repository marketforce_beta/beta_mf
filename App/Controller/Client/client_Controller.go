package Client

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
)

func GetAllClients(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var clients []model.BetaClients
	db.Find(&clients)
	if len(clients) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No clients present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "clients Found", "data": clients})
}

func CreateClient(c *fiber.Ctx) error {
	type clientRequest struct {
		ClientTypeID int    `valid:"required,lte=10" json:"client_type_id" form:"client_type_id"`
		CategoryID   int    `valid:"required,lte=10" json:"category_id" form:"category_id"`
		StateID      int    `valid:"required,lte=10" json:"state_id" form:"state_id"`
		Name         string `valid:"lte=255" json:"name" form:"name"`
		FirstName    string `valid:"lte=255" json:"first_name" form:"first_name"`
		LastName     string `valid:"lte=255" json:"last_name" form:"last_name"`
		Address      string `valid:"lte=255" json:"address" form:"address"`
		City         string `valid:"lte=255" json:"city" form:"city"`
		Zip          string `valid:"lte=255" json:"zip" form:"zip"`
		Phone        string `valid:"lte=255" json:"phone" form:"phone"`
		Email        string `valid:"lte=255" json:"email" form:"email"`
		TaxExempt    string `valid:",lte=1" json:"tax_exempt" form:"tax_exempt"`
		Notes        string `valid:"lte=255" json:"notes" form:"notes"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var client model.BetaClients
	var clientRequestData clientRequest
	if err := c.BodyParser(&clientRequestData); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	client.ClientTypeID = uint(clientRequestData.ClientTypeID)
	client.CategoryID = uint(clientRequestData.CategoryID)
	client.Name = clientRequestData.Name
	client.FirstName = clientRequestData.FirstName
	client.LastName = clientRequestData.LastName
	client.Address = clientRequestData.Address
	client.City = clientRequestData.City
	client.StateID = uint(clientRequestData.StateID)
	client.Zip = clientRequestData.Zip
	client.Phone = clientRequestData.Phone
	client.Email = clientRequestData.Email
	client.Notes = clientRequestData.Notes
	file, err := c.FormFile("uploadFile")
	if err == nil {
		carpet := "Temp/" + data
		if _, err := os.Stat(carpet); os.IsNotExist(err) {
			err = os.Mkdir(carpet, 0755)
			if err != nil {
				return c.JSON(fiber.Map{"status": "error", "message": "Client not created", "data": err})
			}
		}
		route := "Temp/" + data + "/" + time.Now().Format("20060102150405") + "_" + file.Filename
		err = c.SaveFile(file, route)
		if err != nil {
			return c.JSON(fiber.Map{"status": "error", "message": "Client not created", "data": err})
		}
		msg := utils.UploadFile(route, data)
		client.TaxExempt = true
		client.TaxExemptDocument = msg
	}
	client.DateSptUpdated = time.Now()
	client.CategoryUpdated = time.Now()
	db.Create(&client)
	return c.JSON(fiber.Map{"status": "success", "message": "Client Created", "data": client})
}

func GetClient(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var client model.BetaClients
	id := c.Params("id")
	db.First(&client, id)
	if client.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Client not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Client Found", "data": client})
}

func UpdateClient(c *fiber.Ctx) error {
	type clientRequest struct {
		ClientTypeID int    `valid:"required,lte=10" json:"client_type_id" form:"client_type_id"`
		CategoryID   int    `valid:"required,lte=10" json:"category_id" form:"category_id"`
		StateID      int    `valid:"required,lte=10" json:"state_id" form:"state_id"`
		Name         string `valid:"required,lte=255" json:"name" form:"name"`
		FirstName    string `valid:"required,lte=255" json:"first_name" form:"first_name"`
		LastName     string `valid:"required,lte=255" json:"last_name" form:"last_name"`
		Address      string `valid:"required,lte=255" json:"address" form:"address"`
		City         string `valid:"required,lte=255" json:"city" form:"city"`
		Zip          string `valid:"required,lte=255" json:"zip" form:"zip"`
		Phone        string `valid:"required,lte=255" json:"phone" form:"phone"`
		Email        string `valid:"required,lte=255" json:"email" form:"email"`
		Notes        string `valid:"lte=255" json:"notes" form:"notes"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var client model.BetaClients
	var clientRequestData clientRequest
	id := c.Params("id")
	if err := c.BodyParser(&clientRequestData); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Find(&client, id)
	client.ClientTypeID = uint(clientRequestData.ClientTypeID)
	client.CategoryID = uint(clientRequestData.CategoryID)
	client.Name = clientRequestData.Name
	client.FirstName = clientRequestData.FirstName
	client.LastName = clientRequestData.LastName
	client.Address = clientRequestData.Address
	client.City = clientRequestData.City
	client.StateID = uint(clientRequestData.StateID)
	client.Zip = clientRequestData.Zip
	client.Phone = clientRequestData.Phone
	client.Email = clientRequestData.Email
	client.Notes = clientRequestData.Notes
	file, err := c.FormFile("uploadFile")
	if err == nil {
		carpet := "Temp/" + data
		if _, err := os.Stat(carpet); os.IsNotExist(err) {
			err = os.Mkdir(carpet, 0755)
			if err != nil {
				return c.JSON(fiber.Map{"status": "error", "message": "Client not created", "data": err})
			}
		}
		route := "Temp/" + data + "/" + time.Now().Format("20060102150405") + "_" + file.Filename
		err = c.SaveFile(file, route)
		if err != nil {
			return c.JSON(fiber.Map{"status": "error", "message": "Client not created", "data": err})
		}
		msg := utils.UploadFile(route, data)
		client.TaxExempt = true
		client.TaxExemptDocument = msg
	}
	db.Model(&client).Where("id = ?", id).Updates(client)

	return c.JSON(fiber.Map{"status": "success", "message": "Client Updated", "data": client})
}

func DeleteClient(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var client model.BetaClients
	id := c.Params("id")
	db.First(&client, id)
	if client.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Client not found", "data": nil})
	}
	db.Delete(&client)
	return c.JSON(fiber.Map{"status": "success", "message": "Client Deleted", "data": nil})
}

func GetSates(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var states []model.BetaClientStates
	db.Find(&states)
	if len(states) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No states present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "states Found", "data": states})
}

func ChangeCategory(c *fiber.Ctx) error {
	type ClientRequest struct {
		CategoryID int `json:"category_id"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var client model.BetaClients
	var clientRequest ClientRequest
	id := c.Params("id")
	if err := c.BodyParser(&clientRequest); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	client.CategoryID = uint(clientRequest.CategoryID)
	if clientRequest.CategoryID == 1 {
		client.Category = false
	} else {
		client.Category = true
	}
	client.CategoryUpdated = time.Now()
	db.Model(&client).Where("id = ?", id).Updates(client)
	return c.JSON(fiber.Map{"status": "success", "message": "Client Updated", "data": client})
}

func GetFiles(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var files []model.BetaDocument
	var fileExempt model.BetaDocument
	var client model.BetaClients
	id := c.Params("id")
	db.First(&client, id)
	db.Where("client_id = ?", id).Find(&files)
	if client.TaxExempt == true {
		fileExempt.ClientID = client.ID
		fileExempt.Document = client.TaxExemptDocument
		files = append(files, fileExempt)
	}
	if len(files) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No files present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "files Found", "data": files})
}

func UploadCustomFiles(c *fiber.Ctx) error {
	var fileExempt model.BetaDocument
	var client model.BetaClients
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	id := c.Params("id")
	if form, err := c.MultipartForm(); err == nil {
		carpet := "Temp/" + data
		if _, err := os.Stat(carpet); os.IsNotExist(err) {
			err = os.Mkdir(carpet, 0755)
			if err != nil {
				return c.JSON(fiber.Map{"status": "error", "message": "folder not created", "data": err})
			}
		}
		customFiles := form.File["customFiles"]
		if customFiles == nil {
			return c.JSON(fiber.Map{"status": "error", "message": "No files present", "data": nil})
		}
		var dataFiles []string
		for _, file := range customFiles {
			route := "Temp/" + data + "/" + time.Now().Format("20060102150405") + "_" + file.Filename
			err = c.SaveFile(file, route)
			if err != nil {
				return c.JSON(fiber.Map{"status": "error", "message": "file not created", "data": err})
			}
			dataFiles = append(dataFiles, route)
			intID, _ := strconv.Atoi(id)
			var files model.BetaDocument
			files.ClientID = uint(intID)
			files.Document = "https://storage.googleapis.com/allamericanmbs/" + route
			db.Create(&files)
		}
		utils.UploadMultipleFiles(dataFiles)
	} else {
		return c.JSON(fiber.Map{"status": "error", "message": "file not created", "data": err})
	}
	var files []model.BetaDocument
	db.Where("client_id = ?", id).Find(&files)
	db.First(&client, id)
	if client.TaxExempt == true {
		fileExempt.ClientID = client.ID
		fileExempt.Document = client.TaxExemptDocument
		files = append(files, fileExempt)
	}
	return c.JSON(fiber.Map{"status": "success", "message": "files Created", "data": files})
}
