package Colors

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

func GetColors(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var colors []model.BetaColors
	db.Find(&colors)
	if len(colors) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No colors present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Colors Found", "data": colors})
}

func CreateColors(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var color model.BetaColors
	if err := c.BodyParser(&color); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Create(&color)
	return c.JSON(fiber.Map{"status": "success", "message": "Color Created", "data": color})
}

func GetColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var color model.BetaColors
	if err := db.Where("id = ?", c.Params("id")).First(&color).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Color not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Color Found", "data": color})
}

func UpdateColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var color model.BetaColors
	id := c.Params("id")
	if err := c.BodyParser(&color); err != nil {
		return c.Status(400).JSON(fiber.Map{"message": "Error parsing body"})
	}
	db.Model(&color).Where("id = ?", id).Updates(color)
	return c.JSON(fiber.Map{"status": "success", "message": "Color Updated", "data": color})

}

func DeleteColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var color model.BetaColors
	if err := db.Where("id = ?", c.Params("id")).First(&color).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Color not found", "data": nil})
	}
	db.Delete(&color)
	return c.JSON(fiber.Map{"status": "success", "message": "Color Deleted", "data": nil})
}
