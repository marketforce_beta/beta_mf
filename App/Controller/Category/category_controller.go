package Category

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

func GetCategories(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	/*sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()*/
	var categories []model.BetaCategory
	db.Find(&categories)
	if len(categories) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No categories present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Categories found", "data": categories})
}

func CreateCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var category model.BetaCategory
	if err := c.BodyParser(&category); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Create(&category).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Category created", "data": category})
}

func GetCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var category model.BetaCategory
	if err := db.Where("id = ?", c.Params("id")).First(&category).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Category not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Category found", "data": category})
}

func UpdateCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var category model.BetaCategory
	if err := db.Where("id = ?", c.Params("id")).First(&category).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Category not found", "data": nil})
	}
	if err := c.BodyParser(&category); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Save(&category).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Category updated", "data": category})
}

func DeleteCategory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var category model.BetaCategory
	if err := db.Where("id = ?", c.Params("id")).First(&category).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Category not found", "data": nil})
	}
	if err := db.Delete(&category).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Category deleted", "data": nil})
}
