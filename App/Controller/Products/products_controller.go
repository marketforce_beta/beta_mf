package Products

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	utils "gitlab.com/victorrb1015/beta_mf/App/Utils"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
)

type ProductsAcero struct {
	BetaProducts model.BetaProducts
	Category     model.BetaProductsCategory
}

func GetProducts(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var products []model.BetaProducts
	db.Find(&products)
	if len(products) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "success", "message": "No Products Charge", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Products Found", "data": products})
}

func GetAceroProducts(c *fiber.Ctx) error {
	data := "4c762574fdd27543636689b928a914db8af8ba873681c3e22bf62316df9159e6f50859fb61e4065cd35da41bdc"
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var products []model.BetaProducts
	var acero []ProductsAcero
	db.Where("name not like '%Scrap%'").Find(&products)
	if len(products) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "success", "message": "No Products Charge", "data": nil})
	}
	for _, product := range products {
		var category model.BetaProductsCategory
		db.First(&category, product.ProductsCategoryID)
		acero = append(acero, ProductsAcero{product, category})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Products Found", "data": acero})
}

func CreateProduct(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var product model.BetaProducts
	if err := c.BodyParser(&product); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid Body", "data": err})
	}
	db.Create(&product)
	return c.JSON(fiber.Map{"status": "success", "message": "Product Created", "data": product})
}

func GetProduct(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var product model.BetaProducts
	id := c.Params("id")
	db.First(&product, id)
	if product.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Product not found"})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Product Found", "data": product})
}

func UpdateProduct(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var product model.BetaProducts
	var productOld model.BetaProducts
	var productNew model.BetaProducts
	id := c.Params("id")
	if err := c.BodyParser(&product); err != nil {
		return c.Status(400).JSON(fiber.Map{"message": "Error parsing body"})
	}
	db.First(&productOld, id)
	if productOld.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Product not found"})
	}
	productOld.Name = product.Name
	productOld.Description = product.Description
	productOld.Price = product.Price
	productOld.Type = product.Type
	productOld.Weight = product.Weight
	productOld.Length = product.Length
	productOld.Colors = product.Colors
	productOld.Gauge = product.Gauge
	productOld.ProductsCategoryID = product.ProductsCategoryID
	productOld.CatLength = product.CatLength
	productOld.WeightString = product.WeightString
	db.Save(&productOld)
	db.Find(&productNew, id)
	return c.JSON(fiber.Map{"status": "success", "message": "Product Updated", "data": productNew})
}

func DeleteProduct(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var product model.BetaProducts
	id := c.Params("id")
	db.First(&product, id)
	if product.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Product not found"})
	}
	db.Delete(&product)
	return c.JSON(fiber.Map{"status": "success", "message": "Product Deleted"})
}

func CreateCatalogLength(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var catLeng model.BetaCatalogLength
	if err := c.BodyParser(&catLeng); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid Body", "data": err})
	}
	db.Create(&catLeng)
	return c.JSON(fiber.Map{"status": "success", "message": "Catalog Length Created", "data": catLeng})
}

func GetCatalogLengthByID(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var catLeng []model.BetaCatalogLength
	id := c.Params("id")
	db.Where("product_id = ?", id).Find(&catLeng)
	if len(catLeng) == 0 {
		return c.JSON(fiber.Map{"status": "error", "message": "Length not Found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Length Found", "data": catLeng})
}

func DeleteCatalogLength(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var catLeng model.BetaCatalogLength
	id := c.Params("id")
	db.First(&catLeng, id)
	if catLeng.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Catalog Length not found"})
	}
	db.Delete(&catLeng)
	return c.JSON(fiber.Map{"status": "success", "message": "Catalog Length Deleted"})
}

func CreateProductAdditional(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var productAdditional model.BetaProductsAdditional
	if err := c.BodyParser(&productAdditional); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid Body", "data": err})
	}
	db.Create(&productAdditional)
	return c.JSON(fiber.Map{"status": "success", "message": "Product Additional Created", "data": productAdditional})
}

func GetProductAdditionalByID(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var productAdditional []model.BetaProductsAdditional
	id := c.Params("id")
	db.Where("product_id = ?", id).Find(&productAdditional)
	if len(productAdditional) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "success", "message": "No Additional Charge", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Additional Found", "data": productAdditional})
}

func UpdateProductAdditional(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var productAdditional model.BetaProductsAdditional
	var productAdditionalOld model.BetaProductsAdditional
	var productAdditionalNew model.BetaProductsAdditional
	id := c.Params("id")
	if err := c.BodyParser(&productAdditional); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid Body", "data": err})
	}
	db.Find(&productAdditionalOld, id)
	if productAdditionalOld.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Product Additional not found", "status": "error", "data": nil})
	}
	productAdditionalOld.Price = productAdditional.Price
	productAdditionalOld.Name = productAdditional.Name
	db.Save(&productAdditionalOld)
	db.Find(&productAdditionalNew, id)
	return c.JSON(fiber.Map{"status": "success", "message": "Product Additional Updated", "data": productAdditionalNew})
}

func DeleteProductAdditional(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var productAdditional model.BetaProductsAdditional
	id := c.Params("id")
	db.First(&productAdditional, id)
	if productAdditional.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Product Additional not found"})
	}
	db.Delete(&productAdditional)
	return c.JSON(fiber.Map{"status": "success", "message": "Product Additional Deleted"})
}
