package Utils

import (
	"cloud.google.com/go/storage"
	"flag"
	"gitlab.com/victorrb1015/beta_mf/Config"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"io"
	"log"
	"os"
	"time"
)

func UploadFile(FilePath string, Carpet string) string {

	var (
		projectID  = flag.String("project", Config.Config("PROJECT_ID"), "Your cloud project ID.")
		bucketName = flag.String("bucket", Config.Config("BUCKET_NAME"), "The name of an existing bucket within your project.")
		fileName   = flag.String("file", FilePath, "The file to upload.")
	)
	flag.Parse()
	if *bucketName == "" {
		log.Fatalf("Bucket argument is required. See --help.")
	}
	if *projectID == "" {
		log.Fatalf("Project argument is required. See --help.")
	}
	if *fileName == "" {
		log.Fatalf("File argument is required. See --help.")
	}
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile("upheld-beach-343016-c094ea3b5ac1.json"))
	if err != nil {
		return "Could not delete object during cleanup: " + err.Error()
	}
	defer client.Close()
	// Open local file.
	f, err := os.Open(*fileName)
	if err != nil {
		return "os.Open: " + err.Error()
	}
	defer f.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	o := client.Bucket(*bucketName).Object(*fileName)
	o = o.If(storage.Conditions{DoesNotExist: true})
	wc := o.NewWriter(ctx)
	if _, err = io.Copy(wc, f); err != nil {
		return "Error Copy: " + err.Error()
	}
	if err := wc.Close(); err != nil {
		return "Writer.Close: " + err.Error()
	}
	return "https://storage.googleapis.com/allamericanmbs/" + FilePath
}

func UploadMultipleFiles(FilePaths []string) []string {
	bucketName := Config.Config("BUCKET_NAME")
	array := []string{}
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile("upheld-beach-343016-c094ea3b5ac1.json"))
	if err != nil {
		array = append(array, "Could not delete object during cleanup: "+err.Error())
	}
	defer client.Close()

	for _, FilePath := range FilePaths {
		// Open local file.
		f, err := os.Open(FilePath)
		if err != nil {
			array = append(array, "os.Open: "+err.Error())
		}
		defer f.Close()
		ctx, cancel := context.WithTimeout(ctx, time.Second*50)
		defer cancel()
		o := client.Bucket(bucketName).Object(FilePath)
		o = o.If(storage.Conditions{DoesNotExist: true})
		wc := o.NewWriter(ctx)
		if _, err = io.Copy(wc, f); err != nil {
			array = append(array, "Error Copy: "+err.Error())
		}
		if err := wc.Close(); err != nil {
			array = append(array, "Writer.Close: "+err.Error())
		}
	}
	return array
}
