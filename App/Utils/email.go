package Utils

import (
	"log"
	"strconv"

	hermes "github.com/matcornic/hermes/v2"
	mail "github.com/xhit/go-simple-mail/v2"
	config "gitlab.com/victorrb1015/beta_mf/Config"
)

func SendEmail(body hermes.Email, to string, subject string) {
	// Configure hermes by setting a theme and your product info
	h := hermes.Hermes{
		// Optional Theme
		// Theme: new(Default)
		Product: hermes.Product{
			// Appears in header & footer of e-mails
			Name: "MarketForce",
			Link: "allamericanmbs.com",
			// Optional product logo
			Logo: "https://allamericanmbs.com/images/AABMK/Logo_MF.png",
			// Custom copyright notice
			Copyright:   "Copyright © 2022 MarketForce. All rights reserved.",
			TroubleText: "If you’re having trouble, Send Email to info@allamericanmbs.com",
		},
	}
	emailBody, erro := h.GenerateHTML(body)
	if erro != nil {
		panic(erro) // Tip: Handle error with something else than a panic ;)
	}
	port, _ := strconv.Atoi(config.Config("SMTP_Port"))
	server := mail.NewSMTPClient()
	server.Host = config.Config("SMTP_Host")
	server.Port = port
	server.Username = config.Config("SMTP_Username")
	server.Password = config.Config("SMTP_Pass")
	server.Encryption = mail.EncryptionTLS
	smtpClient, err := server.Connect()
	if err != nil {
		log.Fatal(err)
	}
	// Create email
	email := mail.NewMSG()
	email.SetFrom("MarketForce By MBS <info@allamericanmbs.com>")
	email.AddTo(to)
	email.SetSubject(subject)
	email.SetBody(mail.TextHTML, emailBody)
	// Send email
	err = email.Send(smtpClient)
	if err != nil {
		log.Fatal(err)
	}
}

func SentEmailDocument(body hermes.Email, to string, subject string, File string) {
	// Configure hermes by setting a theme and your product info
	h := hermes.Hermes{
		// Optional Theme
		// Theme: new(Default)
		Product: hermes.Product{
			// Appears in header & footer of e-mails
			Name: "MarketForce",
			Link: "acero.industries",
			// Optional product logo
			Logo: "https://storage.googleapis.com/allamericanmbs/assets/Acero.png",
			// Custom copyright notice
			Copyright:   "Copyright © 2022 MarketForce. All rights reserved.",
			TroubleText: "If you’re having trouble, Send Email to info@allamericanmbs.com",
		},
	}
	emailBody, erro := h.GenerateHTML(body)
	if erro != nil {
		panic(erro) // Tip: Handle error with something else than a panic ;)
	}

	port, _ := strconv.Atoi(config.Config("SMTP_PORT"))
	server := mail.NewSMTPClient()
	server.Host = config.Config("SMTP_HOST")
	server.Port = port
	server.Username = config.Config("SMTP_USERNAME")
	server.Password = config.Config("SMTP_PASS")
	server.Encryption = mail.EncryptionTLS

	smtpClient, err := server.Connect()
	if err != nil {
		log.Fatal(err)
	}
	// Create email
	email := mail.NewMSG()
	email.SetFrom("ordertx@acero.industries")
	email.AddTo(to)
	email.SetSubject(subject)
	email.SetBody(mail.TextHTML, emailBody)
	email.AddAttachment(File)
	// Send email
	err = email.Send(smtpClient)
	if err != nil {
		log.Fatal(err)
	}
}
func CreateOrderEmail(client string, clientEmail string, File string, subject string) {
	email := hermes.Email{
		Body: hermes.Body{
			Name: client,
			Intros: []string{
				"Your order has been processed successfully.",
			},
			Outros: []string{
				"Need help, or have questions? Just reply to this email, we'd love to help.",
			},
		},
	}
	SentEmailDocument(email, clientEmail, subject, File)
}

func CreateEstimateEmail(client string, clientEmail string, File string, subject string) {
	email := hermes.Email{
		Body: hermes.Body{
			Name: client,
			Intros: []string{
				"We have received the items that you wish to order",
				"This is your ESTIMATE showing the items you have ordered and the total costs. PLEASE REVIEW THE ESTIMATE to make certain that every detail of your order is correct.",
				"QUANTITY / COLOR / SIZE / GAUGE / STYLE / PRODUCT",
				"Once you CONFIRM that the ESTIMATE IS EXACTLY what you want please reply with APPROVED.",
			},
			Outros: []string{
				"IF you need anything changed PLEASE REPLY with  CHANGES and tell us all that you request and we will resend the ESTIMATE when it is correct.",
				"IF you have decided not to continue with the order, please reply with CANCEL",
				"If you need any assistance please contact us at 972.703.2550 or by emailing ",
				"ORDERSTX@acero.industries",
				"Thank you for your business",
				"Acero Building Components",
				"11250 E Hwy 80",
				"Terrell, TX 75161",
				"ORDERSTX@acero.industries",
				"Thank you for your business",
				"Acero Building Components",
				"11250 E Hwy 80 ",
				"Terrell, TX 75161",
			},
			Signature: " ",
		},
	}
	SentEmailDocument(email, clientEmail, subject, File)
}
