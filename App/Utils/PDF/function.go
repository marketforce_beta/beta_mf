package PDF

import "github.com/johnfercher/maroto/pkg/color"

func GetEstimateColor() color.Color {
	return color.Color{
		Red:   20,
		Green: 99,
		Blue:  145,
	}
}

func GetOrderColor() color.Color {
	return color.Color{
		Red:   21,
		Green: 73,
		Blue:  135,
	}
}

func GetInvoiceColor() color.Color {
	return color.Color{
		Red:   22,
		Green: 189,
		Blue:  205,
	}
}

func GetBillOfLadingColor() color.Color {
	return color.Color{
		Red:   22,
		Green: 171,
		Blue:  171,
	}
}

func GetWorkOrderColor() color.Color {
	return color.Color{
		Red:   254,
		Green: 207,
		Blue:  8,
	}
}

func getHeader() []string {
	return []string{"#", "Item", "Description", "Color", "Qty", "Rate", "Length(FT)", "Price", "Wt."}
}

func GetHeaderWorkOrder() []string {
	return []string{"Item", "Description", "Color", "Qty", "Length(FT)", "Wt."}
}

func GetHeaderBollOfLanding() []string {
	return []string{"#", "Item", "Description", "Color", "Qty", "Length(FT)", "Wt."}
}

func GetDarkGrayColor() color.Color {
	return color.Color{
		Red:   200,
		Green: 200,
		Blue:  200,
	}
}
