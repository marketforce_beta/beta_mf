package PDF

import (
	"fmt"
	"github.com/johnfercher/maroto/pkg/color"
	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"github.com/johnfercher/maroto/pkg/props"
	"time"
)

type CompanyInfo struct {
	Logo        string
	Name        string
	Address     string
	PhoneNumber string
	Email       string
	Web         string
}

type EstimateInfo struct {
	EstimateNumber string
	Date           string
	AmountDue      string
	ExpirationDate string
	ClientName     string
	ClientAddress  string
	ClientPhone    string
	ClientEmail    string
	Weight         string
	Freight        string
	Subtotal       string
	Tax            string
	TaxValue       string
	Total          string
	AmountPaid     string
	Balance        string
	TotalItems     string
}

type Items struct {
	Number      string
	Item        string
	Description string
	Color       string
	Quantity    string
	Rate        string
	Length      string
	Price       string
	Weight      string
}

func CreateEstimate(company CompanyInfo, estimate EstimateInfo, items [][]string, data string, jobtittle string) string {
	estimateColor := GetEstimateColor()
	grayColor := GetDarkGrayColor()
	header := getHeader()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(5, 10, 5)
	m.RegisterHeader(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(10, func() {
			m.Col(12, func() {
				_ = m.FileImage("Assets/Headers/estimate.png", props.Rect{
					Center: true,
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(4, func() {
		})
		m.Row(20, func() {
			m.Col(2, func() {
				m.Text("Estimate:", props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Date:", props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				/*m.Text("Expiration Date:", props.Text{
					Top:         6,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Amount Due:", props.Text{
					Top:         9,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})*/
				m.Text("Client:", props.Text{
					Top:   7,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Tel:", props.Text{
					Top:   10,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Email:", props.Text{
					Top:   13,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Ship To:", props.Text{
					Top:   16,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.Col(3, func() {
				m.Text(estimate.EstimateNumber, props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.Date, props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				}) /*
					m.Text(estimate.ExpirationDate, props.Text{
						Top:         6,
						Size:        8,
						Align:       consts.Left,
						Style:       consts.Bold,
						Extrapolate: false,
					})
					m.Text(estimate.AmountDue, props.Text{
						Top:         9,
						Size:        8,
						Align:       consts.Left,
						Style:       consts.Bold,
						Extrapolate: false,
					})*/
				m.Text(estimate.ClientName, props.Text{
					Top:   7,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientPhone, props.Text{
					Top:   10,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientEmail, props.Text{
					Top:   13,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientAddress, props.Text{
					Top:   16,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(4)
			m.Col(3, func() {
				_ = m.FileImage(company.Logo, props.Rect{
					Center:  true,
					Percent: 110,
				})
			})

		})
	})
	m.RegisterFooter(func() {
		m.Row(20, func() {
			m.Col(3, func() {
				m.Text("Thank you for choosing "+company.Name, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(5)
			m.Col(4, func() {
				m.Text(company.PhoneNumber, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(company.Web, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   3,
				})
				m.Text(company.Email, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   6,
				})
				m.Text(company.Address, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   9,
				})
			})
		})
	})
	m.Row(5, func() {})
	m.SetBackgroundColor(estimateColor)
	m.Row(3, func() {
		m.Col(3, func() {
			m.Text(" ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
		m.ColSpace(9)
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, items, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{1, 2, 3, 1, 1, 1, 1, 1, 1},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{1, 2, 3, 1, 1, 1, 1, 1, 1},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Weight :", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text(estimate.Weight+" lbs", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Freight:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Freight, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("SubTotal:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Subtotal, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Tax %"+estimate.Tax+":", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.TaxValue, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Total:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Total, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Amount Paid:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.AmountPaid, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Balance Due:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Balance, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(15, func() {
		m.Col(12, func() {
			m.Text("Job Titles: "+jobtittle, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Left,
			})
		})
	})
	name := "Temp/" + data + "/" + "estimate_" + time.Now().Format("20060102150405") + "_" + estimate.EstimateNumber + ".pdf"
	err := m.OutputFileAndClose(name)
	if err != nil {
		fmt.Println("Could not save PDF:", err)
	}
	return name
}

func CreateOrder(company CompanyInfo, estimate EstimateInfo, items [][]string, data string) string {
	estimateColor := GetOrderColor()
	grayColor := GetDarkGrayColor()
	header := getHeader()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(5, 10, 5)
	m.RegisterHeader(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(10, func() {
			m.Col(12, func() {
				_ = m.FileImage("Assets/Headers/order.png", props.Rect{
					Center: true,
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(4, func() {
		})
		m.Row(20, func() {
			m.Col(2, func() {
				m.Text("Order:", props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Date:", props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				/*m.Text("Expiration Date:", props.Text{
					Top:         6,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Amount Due:", props.Text{
					Top:         9,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})*/
				m.Text("Client:", props.Text{
					Top:   7,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Tel:", props.Text{
					Top:   10,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Email:", props.Text{
					Top:   13,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Ship To:", props.Text{
					Top:   16,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.Col(3, func() {
				m.Text(estimate.EstimateNumber, props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.Date, props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				}) /*
					m.Text(estimate.ExpirationDate, props.Text{
						Top:         6,
						Size:        8,
						Align:       consts.Left,
						Style:       consts.Bold,
						Extrapolate: false,
					})
					m.Text(estimate.AmountDue, props.Text{
						Top:         9,
						Size:        8,
						Align:       consts.Left,
						Style:       consts.Bold,
						Extrapolate: false,
					})*/
				m.Text(estimate.ClientName, props.Text{
					Top:   7,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientPhone, props.Text{
					Top:   10,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientEmail, props.Text{
					Top:   13,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientAddress, props.Text{
					Top:   16,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(4)
			m.Col(3, func() {
				_ = m.FileImage(company.Logo, props.Rect{
					Center:  true,
					Percent: 110,
				})
			})

		})
	})
	m.RegisterFooter(func() {
		m.Row(20, func() {
			m.Col(3, func() {
				m.Text("Thank you for choosing "+company.Name, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(5)
			m.Col(4, func() {
				m.Text(company.PhoneNumber, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(company.Web, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   3,
				})
				m.Text(company.Email, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   6,
				})
				m.Text(company.Address, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   9,
				})
			})
		})
	})
	m.Row(5, func() {})
	m.SetBackgroundColor(estimateColor)
	m.Row(3, func() {
		m.Col(3, func() {
			m.Text(" ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
		m.ColSpace(9)
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, items, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{1, 2, 3, 1, 1, 1, 1, 1, 1},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{1, 2, 3, 1, 1, 1, 1, 1, 1},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Weight :", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text(estimate.Weight+" lbs", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Freight:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Freight, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("SubTotal:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Subtotal, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Tax %"+estimate.Tax+":", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.TaxValue, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Total:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Total, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Amount Paid:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.AmountPaid, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Balance Due:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Balance, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	name := "Temp/" + data + "/" + "order_" + time.Now().Format("20060102150405") + "_" + estimate.EstimateNumber + ".pdf"
	err := m.OutputFileAndClose(name)
	if err != nil {
		fmt.Println("Could not save PDF:", err)
	}
	return name
}

func CreateInvoice(company CompanyInfo, estimate EstimateInfo, items [][]string, data string, jobtittle string) string {
	estimateColor := GetInvoiceColor()
	grayColor := GetDarkGrayColor()
	header := getHeader()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(5, 10, 5)
	m.RegisterHeader(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(10, func() {
			m.Col(12, func() {
				_ = m.FileImage("Assets/Headers/invoice.png", props.Rect{
					Center: true,
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(4, func() {
		})
		m.Row(20, func() {
			m.Col(2, func() {
				m.Text("Invoice:", props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Date:", props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				/*m.Text("Expiration Date:", props.Text{
					Top:         6,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Amount Due:", props.Text{
					Top:         9,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})*/
				m.Text("Client:", props.Text{
					Top:   7,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Tel:", props.Text{
					Top:   10,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Email:", props.Text{
					Top:   13,
					Size:  8,
					Align: consts.Left,
				})
				m.Text("Ship To:", props.Text{
					Top:   16,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.Col(3, func() {
				m.Text(estimate.EstimateNumber, props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.Date, props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				}) /*
					m.Text(estimate.ExpirationDate, props.Text{
						Top:         6,
						Size:        8,
						Align:       consts.Left,
						Style:       consts.Bold,
						Extrapolate: false,
					})
					m.Text(estimate.AmountDue, props.Text{
						Top:         9,
						Size:        8,
						Align:       consts.Left,
						Style:       consts.Bold,
						Extrapolate: false,
					})*/
				m.Text(estimate.ClientName, props.Text{
					Top:   7,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientPhone, props.Text{
					Top:   10,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientEmail, props.Text{
					Top:   13,
					Size:  8,
					Align: consts.Left,
				})
				m.Text(estimate.ClientAddress, props.Text{
					Top:   16,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(4)
			m.Col(3, func() {
				_ = m.FileImage(company.Logo, props.Rect{
					Center:  true,
					Percent: 110,
				})
			})

		})
	})
	m.RegisterFooter(func() {
		m.Row(20, func() {
			m.Col(3, func() {
				m.Text("Thank you for choosing "+company.Name, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(5)
			m.Col(4, func() {
				m.Text(company.PhoneNumber, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(company.Web, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   3,
				})
				m.Text(company.Email, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   6,
				})
				m.Text(company.Address, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   9,
				})
			})
		})
	})
	m.Row(5, func() {})
	m.SetBackgroundColor(estimateColor)
	m.Row(3, func() {
		m.Col(3, func() {
			m.Text(" ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
		m.ColSpace(9)
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, items, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{1, 2, 3, 1, 1, 1, 1, 1, 1},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{1, 2, 3, 1, 1, 1, 1, 1, 1},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.Row(5, func() {
		m.ColSpace(7)

		m.Col(2, func() {
			m.Text("Weight :", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text(estimate.Weight+" lbs", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Freight:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Freight, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("SubTotal:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Subtotal, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Tax %"+estimate.Tax+":", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.TaxValue, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Total:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Total, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Amount Paid:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.AmountPaid, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Balance Due:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Balance, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(15, func() {
		m.Col(12, func() {
			m.Text("Job Titles: "+jobtittle, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Left,
			})
		})
	})
	name := "Temp/" + data + "/" + "invoice_" + time.Now().Format("20060102150405") + "_" + estimate.EstimateNumber + ".pdf"
	err := m.OutputFileAndClose(name)
	if err != nil {
		fmt.Println("Could not save PDF:", err)
	}
	return name
}

func CreateBillOfLanding(company CompanyInfo, estimate EstimateInfo, items [][]string, data string) string {
	estimateColor := GetBillOfLadingColor()
	grayColor := GetDarkGrayColor()
	header := GetHeaderBollOfLanding()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(5, 10, 5)
	m.RegisterHeader(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(10, func() {
			m.Col(12, func() {
				_ = m.FileImage("Assets/Headers/bill_of_landing.png", props.Rect{
					Center: true,
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(4, func() {
		})
		m.Row(20, func() {
			m.Col(2, func() {
				m.Text("Invoice:", props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Date:", props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Ship To:", props.Text{
					Top:   10,
					Size:  10,
					Style: consts.Bold,
					Align: consts.Left,
				})
			})
			m.Col(3, func() {
				m.Text(estimate.EstimateNumber, props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.Date, props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.ClientAddress, props.Text{
					Top:   10,
					Size:  10,
					Style: consts.Bold,
					Align: consts.Left,
				})
			})
			m.ColSpace(4)
			m.Col(3, func() {
				_ = m.FileImage(company.Logo, props.Rect{
					Center:  true,
					Percent: 110,
				})
			})

		})
	})
	m.RegisterFooter(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(7, func() {
			m.Col(12, func() {
				m.Text("Shipping Information", props.Text{
					Top:   1.5,
					Size:  9,
					Style: consts.Bold,
					Align: consts.Center,
					Color: color.NewWhite(),
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(20, func() {
			m.Col(3, func() {
				m.Signature("Driver Signature", props.Font{
					Size:  8,
					Style: consts.BoldItalic,
				})
			})
			m.Col(3, func() {
				m.Signature("Date / Time", props.Font{
					Size:  8,
					Style: consts.BoldItalic,
				})
			})
			m.Col(3, func() {
				m.Signature("Received By", props.Font{
					Size:  8,
					Style: consts.BoldItalic,
				})
			})
			m.Col(3, func() {
				m.Signature("Date / Time", props.Font{
					Size:  8,
					Style: consts.BoldItalic,
				})
			})
		})
		m.Row(7, func() {
			m.Col(12, func() {
				m.Text(" ")
			})
		})
		m.Row(20, func() {
			m.Col(3, func() {
				m.Text("Thank you for choosing "+company.Name, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(5)
			m.Col(4, func() {
				m.Text(company.PhoneNumber, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(company.Web, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   3,
				})
				m.Text(company.Email, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   6,
				})
				m.Text(company.Address, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   9,
				})
			})
		})
	})
	m.Row(5, func() {})
	m.SetBackgroundColor(estimateColor)
	m.Row(3, func() {
		m.Col(3, func() {
			m.Text(" ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
		m.ColSpace(9)
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, items, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{1, 3, 3, 1, 1, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{1, 3, 3, 1, 1, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Total # Of Items:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text(estimate.TotalItems, props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	m.Row(5, func() {
		m.ColSpace(7)
		m.Col(2, func() {
			m.Text("Total Weight:", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Right,
			})
		})
		m.Col(3, func() {
			m.Text("$ "+estimate.Weight+" lbs", props.Text{
				Top:   5,
				Style: consts.Bold,
				Size:  8,
				Align: consts.Center,
			})
		})
	})
	name := "Temp/" + data + "/" + "bill_of_landing_" + time.Now().Format("20060102150405") + "_" + estimate.EstimateNumber + ".pdf"
	err := m.OutputFileAndClose(name)
	if err != nil {
		fmt.Println("Could not save PDF:", err)
	}
	return name
}

func CreateWorkOrderJobPack(company CompanyInfo, estimate EstimateInfo, items [][]string, data string) string {
	estimateColor := GetWorkOrderColor()
	grayColor := GetDarkGrayColor()
	header := GetHeaderWorkOrder()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(5, 10, 5)
	m.RegisterHeader(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(10, func() {
			m.Col(12, func() {
				_ = m.FileImage("Assets/Headers/work_order.png", props.Rect{
					Center: true,
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(4, func() {
		})
		m.Row(20, func() {
			m.Col(2, func() {
				m.Text("Invoice:", props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Date:", props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Client:", props.Text{
					Top:   7,
					Size:  12,
					Align: consts.Left,
				})
			})
			m.Col(3, func() {
				m.Text(estimate.EstimateNumber, props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.Date, props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.ClientName, props.Text{
					Top:   7,
					Size:  12,
					Align: consts.Left,
				})
			})
			m.ColSpace(4)
			m.Col(3, func() {
				_ = m.FileImage(company.Logo, props.Rect{
					Center:  true,
					Percent: 110,
				})
			})

		})
	})
	m.RegisterFooter(func() {
		m.Row(20, func() {
			m.Col(3, func() {
				m.Text("Thank you for choosing "+company.Name, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(5)
			m.Col(4, func() {
				m.Text(company.PhoneNumber, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(company.Web, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   3,
				})
				m.Text(company.Email, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   6,
				})
				m.Text(company.Address, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   9,
				})
			})
		})
	})
	m.Row(5, func() {})
	m.SetBackgroundColor(estimateColor)
	m.Row(3, func() {
		m.Col(3, func() {
			m.Text(" ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
		m.ColSpace(9)
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, items, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	name := "Temp/" + data + "/" + "work_order_" + time.Now().Format("20060102150405") + "_" + estimate.EstimateNumber + ".pdf"
	err := m.OutputFileAndClose(name)
	if err != nil {
		fmt.Println("Could not save PDF:", err)
	}
	return name
}

func CreateWorkOrderPDF(company CompanyInfo, estimate EstimateInfo, itemsTrim [][]string, itemsTubing [][]string, itemsSheet [][]string, itemsRoll [][]string, itemsProducts [][]string, itemsAccessories [][]string, itemsHatChannel [][]string, itemsWalkInDoor [][]string, data string) string {
	estimateColor := GetWorkOrderColor()
	grayColor := GetDarkGrayColor()
	header := GetHeaderWorkOrder()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(5, 10, 5)
	m.RegisterHeader(func() {
		m.SetBackgroundColor(estimateColor)
		m.Row(10, func() {
			m.Col(12, func() {
				_ = m.FileImage("Assets/Headers/work_order.png", props.Rect{
					Center: true,
				})
			})
		})
		m.SetBackgroundColor(color.NewWhite())
		m.Row(4, func() {
		})
		m.Row(20, func() {
			m.Col(2, func() {
				m.Text("Invoice:", props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Date:", props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text("Client:", props.Text{
					Top:   7,
					Size:  12,
					Align: consts.Left,
				})
			})
			m.Col(3, func() {
				m.Text(estimate.EstimateNumber, props.Text{
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.Date, props.Text{
					Top:         3,
					Size:        8,
					Align:       consts.Left,
					Style:       consts.Bold,
					Extrapolate: false,
				})
				m.Text(estimate.ClientName, props.Text{
					Top:   7,
					Size:  12,
					Align: consts.Left,
				})
			})
			m.ColSpace(4)
			m.Col(3, func() {
				_ = m.FileImage(company.Logo, props.Rect{
					Center:  true,
					Percent: 110,
				})
			})

		})
	})
	m.RegisterFooter(func() {
		m.Row(20, func() {
			m.Col(3, func() {
				m.Text("Thank you for choosing "+company.Name, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Left,
				})
			})
			m.ColSpace(5)
			m.Col(4, func() {
				m.Text(company.PhoneNumber, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(company.Web, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   3,
				})
				m.Text(company.Email, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   6,
				})
				m.Text(company.Address, props.Text{
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
					Top:   9,
				})
			})
		})
	})
	m.Row(5, func() {})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Trim", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsTrim, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Tubing", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsTubing, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Sheet Metal", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsSheet, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Roll Up Door", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsRoll, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Products", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsProducts, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Accessories", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsAccessories, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Hat Channel/Braces ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsHatChannel, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	m.SetBackgroundColor(estimateColor)
	m.Row(7, func() {
		m.Col(12, func() {
			m.Text("Walk-In Door", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
				Color: color.NewWhite(),
			})
		})
	})
	m.SetBackgroundColor(color.NewWhite())
	m.TableList(header, itemsWalkInDoor, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      9,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		ContentProp: props.TableListContent{
			Size:      8,
			GridSizes: []uint{3, 3, 1, 2, 1, 2},
		},
		Align:                consts.Center,
		AlternatedBackground: &grayColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})
	name := "Temp/" + data + "/" + "work_order_" + time.Now().Format("20060102150405") + "_" + estimate.EstimateNumber + ".pdf"
	err := m.OutputFileAndClose(name)
	if err != nil {
		fmt.Println("Could not save PDF:", err)
	}
	return name
}
