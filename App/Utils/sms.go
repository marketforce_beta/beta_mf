package Utils

import (
	"crypto/tls"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/twilio/twilio-go"
	openapi "github.com/twilio/twilio-go/rest/api/v2010"
	"github.com/xhit/go-simple-mail/v2"
	config "gitlab.com/victorrb1015/beta_mf/Config"
)

func SendSMS() {
	to := "+527737365844"
	//to := "+525543833721"
	phoneNumber := config.Config("TWILIO_PHONE_NUMBER")
	//accountSid := config.Config("ACCOUNT_SID")
	//authToken := config.Config("AUTH_TOKEN")

	client := twilio.NewRestClient()
	
	params := &openapi.CreateMessageParams{}
	params.SetTo(to)
	params.SetFrom(phoneNumber)
	params.SetBody("Hello from Golang")

	_, err := client.Api.CreateMessage(params)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Message sent")
	}


}

func SendSMSEmail() {
	server := mail.NewSMTPClient()

	// SMTP Server
	server.Host = config.Config("SMTP_Host")
	server.Port, _ = strconv.Atoi(config.Config("SMTP_Port"))
	server.Username = config.Config("SMTP_Username")
	server.Password = config.Config("SMTP_Pass")
	server.Encryption = mail.EncryptionTLS

	// Since v2.3.0 you can specified authentication type:
	// - PLAIN (default)
	// - LOGIN
	// - CRAM-MD5
	// - None
	// server.Authentication = mail.AuthPlain

	// Variable to keep alive connection
	server.KeepAlive = false

	// Timeout for connect to SMTP Server
	server.ConnectTimeout = 10 * time.Second

	// Timeout for send the data and wait respond
	server.SendTimeout = 10 * time.Second

	// Set TLSConfig to provide custom TLS configuration. For example,
	// to skip TLS verification (useful for testing):
	server.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// SMTP client
	smtpClient,err := server.Connect()

	if err != nil{
		log.Fatal(err)
	}

	// New email simple html with inline and CC
	
	email := mail.NewMSG()
	email.SetFrom("MarketForce By MBS <mf@allamericanmbs.com>").
		AddTo("4698872868@vtext.com").
		SetSubject("New Go Email to text")
	//email.SetBody(mail.TextHTML, "<h1>Hello World</h1>")


	email.SetBody(mail.TextPlain, "Hello World from Golang")

	// also you can add body from []byte with SetBodyData, example:
	// email.SetBodyData(mail.TextHTML, []byte(htmlBody))
	// or alternative part
	// email.AddAlternativeData(mail.TextHTML, []byte(htmlBody))

	// add inline
	//email.Attach(&mail.File{FilePath: "/path/to/image.png", Name:"Gopher.png", Inline: true})

	// you can add dkim signature to the email. 
	// to add dkim, you need a private key already created one.
	/*if privateKey != "" {
		options := dkim.NewSigOptions()
		options.PrivateKey = []byte(privateKey)
		options.Domain = "example.com"
		options.Selector = "default"
		options.SignatureExpireIn = 3600
		options.Headers = []string{"from", "date", "mime-version", "received", "received"}
		options.AddSignatureTimestamp = true
		options.Canonicalization = "relaxed/relaxed"

		email.SetDkim(options)
	}*/

	// always check error after send
	if email.Error != nil{
		log.Fatal(email.Error)
	}

	// Call Send and pass the client
	err = email.Send(smtpClient)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Email Sent")
	}
}
