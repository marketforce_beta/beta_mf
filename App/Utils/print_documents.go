package Utils

import (
	"fmt"
	humanize "github.com/dustin/go-humanize"
	"gitlab.com/victorrb1015/beta_mf/App/Model"
	"gitlab.com/victorrb1015/beta_mf/App/Utils/PDF"
	database "gitlab.com/victorrb1015/beta_mf/Database"
	"log"
	"os"
	"strconv"
)

type Company struct {
	FullName     string `db:"full_name" json:"full_name"`
	Address      string `db:"address" json:"address"`
	Abbreviation string `db:"abbreviation" json:"abbreviation"`
	Logo         string `db:"logo" json:"logo" validate:"required"`
	OrdersLogo   string `db:"orders_logo" json:"orders_logo"`
	Country      string `db:"country" json:"country"`
	City         string `db:"city" json:"city"`
	State        string `db:"state" json:"state"`
	ZipCode      string `db:"zip_code" json:"zip_code"`
	EmailFrom    string `db:"email_from" json:"email_from"`
	Phone        string `db:"phone" json:"phone"`
	Database     string `db:"company_db" json:"database"`
	WebSite      string `db:"website" json:"website"`
}

func PrintDocuments(data string, id uint, FName string, LName string) string {
	ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order Model.BetaOrders
	var orderDetails []Model.BetaOrderDetails
	var orderStatus Model.BetaOrderStatusDetail
	var company Company
	var Client Model.BetaClients
	db.Where("id = ?", id).First(&order)
	if order.ID == 0 {
		return "No Order present"
	}
	db.Where("order_id = ?", order.ID).Last(&orderStatus)
	db.Where("order_id = ?", order.ID).Find(&orderDetails)
	db.Where("id = ?", order.ClientID).First(&Client)
	database.ConnectDB()
	alphaDB := database.DB
	alphaDB.Where("company_db = ?", data).First(&company)
	var companyInfo PDF.CompanyInfo
	companyInfo.Logo = "Assets/Companies/assets_Acero.png"
	companyInfo.Name = company.FullName
	companyInfo.Address = company.Address
	companyInfo.Web = company.WebSite
	companyInfo.PhoneNumber = company.Phone
	companyInfo.Email = company.EmailFrom
	var estimateInfo PDF.EstimateInfo
	estimateInfo.EstimateNumber = strconv.Itoa(int(order.OrderNumber))
	estimateInfo.Date = order.OrderDate.Format("01/02/2006")
	estimateInfo.AmountDue = fmt.Sprintf("%.2f", order.BalanceDue)
	estimateInfo.ExpirationDate = order.ExpirationDate.Format("01/02/2006")
	if Client.ID == 1 {
		estimateInfo.ClientName = "Walking Customer: " + FName + " " + LName
	} else {
		if Client.Name != "" {
			estimateInfo.ClientName = Client.Name
		} else {
			estimateInfo.ClientName = Client.FirstName + " " + Client.LastName
		}
		if order.AddressID != 0 {
			var address Model.BetaClientAddress
			var state Model.BetaClientAddressStates
			db.Where("id = ?", order.AddressID).First(&address)
			db.Where("id = ?", address.StateID).First(&state)
			estimateInfo.ClientAddress = address.Address + " " + address.City + ", " + state.Name + " " + address.Zip
		} else {
			var state Model.BetaClientAddressStates
			db.Where("id = ?", Client.StateID).First(&state)
			estimateInfo.ClientAddress = Client.Address + " " + Client.City + ", " + state.Name + " " + Client.Zip
		}
	}
	estimateInfo.ClientPhone = Client.Phone
	estimateInfo.ClientEmail = Client.Email
	estimateInfo.Weight = fmt.Sprintf(humanize.CommafWithDigits(order.TotalWeight, 2))
	estimateInfo.Freight = fmt.Sprintf(humanize.CommafWithDigits(order.Freight, 2))
	estimateInfo.Subtotal = fmt.Sprintf(humanize.CommafWithDigits(order.SubTotal, 2))
	estimateInfo.Tax = fmt.Sprintf(humanize.CommafWithDigits(order.Tax, 2))
	estimateInfo.TaxValue = fmt.Sprintf(humanize.CommafWithDigits(order.TaxTotal, 2))
	estimateInfo.Total = fmt.Sprintf(humanize.CommafWithDigits(order.Total, 2))
	estimateInfo.AmountPaid = fmt.Sprintf(humanize.CommafWithDigits(order.AmountPaid, 2))
	estimateInfo.Balance = fmt.Sprintf(humanize.CommafWithDigits(order.BalanceDue, 2))
	var items [][]string
	var itemsBOL [][]string
	var itemsWork [][]string
	var itemsTrim [][]string
	var itemsTubing [][]string
	var itemsSheet [][]string
	var itemsRoll [][]string
	var itemsProducts [][]string
	var itemsAccessories [][]string
	var itemsHatChannel [][]string
	var itemsWalkInDoor [][]string
	var qty int64
	var pdf5 string
	for i, v := range orderDetails {
		var product Model.BetaProducts
		var item PDF.Items
		var color Model.BetaColors
		db.Find(&product, v.ProductID)
		db.Find(&color, v.ColorID)
		item.Item = product.Name
		item.Description = product.Description
		item.Quantity = strconv.Itoa(int(v.Quantity))
		qty = qty + int64(v.Quantity)
		item.Rate = fmt.Sprintf("%.2f", v.Rate)
		if v.Price != 0 {
			item.Price = fmt.Sprintf("%.2f", v.Price)
		} else {
			item.Price = fmt.Sprintf("%.2f", v.Rate*float64(v.Quantity))
		}
		item.Weight = fmt.Sprintf("%.2f", v.Weight)
		item.Color = color.Name
		switch v.Fraction {
		case 0:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c", v.Length, 39, v.Inches, 34)
		case 0.01:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 1/8 Inch", v.Length, 39, v.Inches, 34)
		case 0.02:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 1/4 Inch", v.Length, 39, v.Inches, 34)
		case 0.03:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 3/8 Inch", v.Length, 39, v.Inches, 34)
		case 0.5:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 1/2 Inch", v.Length, 39, v.Inches, 34)
		case 0.6:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 5/8 Inch", v.Length, 39, v.Inches, 34)
		case 0.7:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 3/4 Inch", v.Length, 39, v.Inches, 34)
		case 0.8:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 7/8 Inch", v.Length, 39, v.Inches, 34)
		}
		itemJson := []string{strconv.Itoa(i + 1), item.Item, item.Description, item.Color, item.Quantity, item.Rate, item.Length, item.Price, item.Weight}
		itemJson2 := []string{strconv.Itoa(i + 1), item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
		items = append(items, itemJson)
		itemsBOL = append(itemsBOL, itemJson2)
	}
	if order.JobPack == false {
		db.Raw("SELECT A.* FROM beta_order_details as A, beta_products as B WHERE A.product_id = B.id AND A.deleted_at IS NULL AND A.order_id = ? ORDER BY B.products_category_id ASC", order.ID).Scan(&orderDetails)
	}
	for _, v := range orderDetails {
		var product Model.BetaProducts
		var item PDF.Items
		var color Model.BetaColors
		db.Find(&product, v.ProductID)
		db.Find(&color, v.ColorID)
		item.Item = product.Name
		item.Description = product.Description
		item.Quantity = strconv.Itoa(int(v.Quantity))
		qty = qty + int64(v.Quantity)
		item.Rate = fmt.Sprintf("%.2f", v.Rate)
		item.Price = fmt.Sprintf("%.2f", v.Price)
		item.Weight = fmt.Sprintf("%.2f", v.Weight)
		item.Color = color.Name
		switch v.Fraction {
		case 0:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c", v.Length, 39, v.Inches, 34)
		case 0.01:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 1/8 Inch", v.Length, 39, v.Inches, 34)
		case 0.02:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 1/4 Inch", v.Length, 39, v.Inches, 34)
		case 0.03:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 3/8 Inch", v.Length, 39, v.Inches, 34)
		case 0.5:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 1/2 Inch", v.Length, 39, v.Inches, 34)
		case 0.6:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 5/8 Inch", v.Length, 39, v.Inches, 34)
		case 0.7:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 3/4 Inch", v.Length, 39, v.Inches, 34)
		case 0.8:
			item.Length = fmt.Sprintf("%.2f %c x %.2f %c - 7/8 Inch", v.Length, 39, v.Inches, 34)
		}
		itemJson := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
		itemsWork = append(itemsWork, itemJson)
		switch product.ProductsCategoryID {
		case 1:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsTrim = append(itemsTrim, itemJson2)
		case 2:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsTubing = append(itemsTubing, itemJson2)
		case 3:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsSheet = append(itemsSheet, itemJson2)
		case 4:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsRoll = append(itemsRoll, itemJson2)
		case 5:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsProducts = append(itemsProducts, itemJson2)
		case 6:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsAccessories = append(itemsAccessories, itemJson2)
		case 7:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsHatChannel = append(itemsHatChannel, itemJson2)
		case 8:
			itemJson2 := []string{item.Item, item.Description, item.Color, item.Quantity, item.Length, item.Weight}
			itemsWalkInDoor = append(itemsWalkInDoor, itemJson2)
		}
	}
	estimateInfo.TotalItems = strconv.Itoa(int(qty))
	carpet := "Temp/" + data
	if _, err := os.Stat(carpet); os.IsNotExist(err) {
		err = os.Mkdir(carpet, 0755)
		if err != nil {
			return "Error creating carpet"
		}
	}
	var document Model.BetaOrdersDocuments
	pdf1 := PDF.CreateEstimate(companyInfo, estimateInfo, items, data, order.Note)
	pdf2 := PDF.CreateOrder(companyInfo, estimateInfo, items, data)
	pdf3 := PDF.CreateInvoice(companyInfo, estimateInfo, items, data, order.Note)
	pdf4 := PDF.CreateBillOfLanding(companyInfo, estimateInfo, itemsBOL, data)
	if order.JobPack == false {
		pdf5 = PDF.CreateWorkOrderPDF(companyInfo, estimateInfo, itemsTrim, itemsTubing, itemsSheet, itemsRoll, itemsProducts, itemsAccessories, itemsHatChannel, itemsWalkInDoor, data)
	}
	if order.JobPack == true {
		pdf5 = PDF.CreateWorkOrderJobPack(companyInfo, estimateInfo, itemsWork, data)
	}
	UploadMultipleFiles([]string{pdf1, pdf2, pdf3, pdf4, pdf5})
	document.Type = "Estimate"
	document.OrderID = order.ID
	document.Document = "https://storage.googleapis.com/allamericanmbs/" + pdf1
	db.Create(&document)
	var doc2 Model.BetaOrdersDocuments
	doc2.Type = "Order"
	doc2.OrderID = order.ID
	doc2.Document = "https://storage.googleapis.com/allamericanmbs/" + pdf2
	db.Create(&doc2)
	var doc3 Model.BetaOrdersDocuments
	doc3.Type = "Invoice"
	doc3.OrderID = order.ID
	doc3.Document = "https://storage.googleapis.com/allamericanmbs/" + pdf3
	db.Create(&doc3)
	var doc4 Model.BetaOrdersDocuments
	doc4.Type = "Bill of Landing"
	doc4.OrderID = order.ID
	doc4.Document = "https://storage.googleapis.com/allamericanmbs/" + pdf4
	db.Create(&doc4)
	var doc5 Model.BetaOrdersDocuments
	doc5.Type = "Work Order"
	doc5.OrderID = order.ID
	doc5.Document = "https://storage.googleapis.com/allamericanmbs/" + pdf5
	db.Create(&doc5)
	return "Success"
}
