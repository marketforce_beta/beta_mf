package Category

import (
	"github.com/gofiber/fiber/v2"
	categoriesController "gitlab.com/victorrb1015/beta_mf/App/Controller/Category"
)

func SetupCategoriesRoutes(router fiber.Router) {
	categories := router.Group("/categories")
	// Read all categories
	categories.Get("/:database", categoriesController.GetCategories)
	// Create a new category
	categories.Post("/:database", categoriesController.CreateCategory)
	// Read a category
	categories.Get("/:database/:id", categoriesController.GetCategory)
	// Update a category
	categories.Put("/:database/:id", categoriesController.UpdateCategory)
	// Delete a category
	categories.Delete("/:database/:id", categoriesController.DeleteCategory)
}
