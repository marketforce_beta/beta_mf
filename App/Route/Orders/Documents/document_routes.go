package Documents

import (
	"github.com/gofiber/fiber/v2"
	ordersDocumentController "gitlab.com/victorrb1015/beta_mf/App/Controller/Orders/Documents"
)

func SetupOrderDocumentsRoutes(router fiber.Router) {
	documents := router.Group("/orders/documents")
	// Print Estimate
	documents.Get("/print/:database/:id", ordersDocumentController.Print)
	// Send Estimate
	documents.Get("/send_estimate/:database/:id", ordersDocumentController.SendEstimate)
}
