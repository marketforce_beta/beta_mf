package Status

import (
	"github.com/gofiber/fiber/v2"
	statusOderController "gitlab.com/victorrb1015/beta_mf/App/Controller/Orders/Status"
)

func SetupOrderStatusRoutes(router fiber.Router) {
	status := router.Group("/order/status")
	//Get status of an order
	status.Get("/:database/:id", statusOderController.GetOrderStatus)
	//Create status of an order
	status.Post("/:database/:id", statusOderController.CreateOrderStatus)
	//Update status of an order
	status.Put("/:database/:id", statusOderController.UpdateOrderStatus)
	//Delete status of an order
	status.Delete("/:database/:id", statusOderController.DeleteOrderStatus)
}
