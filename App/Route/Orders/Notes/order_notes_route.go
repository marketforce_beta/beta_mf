package Notes

import (
	"github.com/gofiber/fiber/v2"
	ordersNoteController "gitlab.com/victorrb1015/beta_mf/App/Controller/Orders/Notes"
)

func SetupOrderNotesRoute(router fiber.Router) {
	note := router.Group("/order/notes")
	// Create Notes
	note.Post("/:database", ordersNoteController.CreateOrderNotes)
	// Get All Notes
	note.Get("/:database/:idOrder", ordersNoteController.GetAllOrderNotes)
	// Update Notes
	note.Put("/:database/:id", ordersNoteController.UpdateOrderNotes)
	// Delete Notes
	note.Delete("/:database/:id", ordersNoteController.DeleteOrderNotes)
}
