package Orders

import (
	"github.com/gofiber/fiber/v2"
	ordersController "gitlab.com/victorrb1015/beta_mf/App/Controller/Orders"
)

func SetupOrdersRoutes(router fiber.Router) {
	orders := router.Group("/orders")
	// Get All Orders
	orders.Get("/:database", ordersController.GetOrders)
	// Get Order By ID
	orders.Get("/:database/:id", ordersController.GetOrder)
	// Create Order
	orders.Post("/:database", ordersController.CreateOrder)
	// Update Order
	orders.Put("/:database/:id", ordersController.UpdateOrder)
	// Delete Order
	orders.Delete("/:database/:id", ordersController.DeleteOrder)

	products := router.Group("/order/p_client")
	// Get All Products
	products.Get("/:database/:id", ordersController.GetProductsByClient)

	cat := router.Group("/order/l_catalog")
	// Get All length
	cat.Get("/:database/:id", ordersController.GetCatalogLeg)

	add := router.Group("/order/p_add")
	// Get All Additional Products
	add.Get("/:database/:id", ordersController.GetAdditionalProduct)

	or := router.Group("/order/order_number")
	// Get Order Number
	or.Get("/:database", ordersController.GetNewInvoiceNumber)

	pay := router.Group("/order/payment_terms")
	// Get Payment Terms
	pay.Get("/:database", ordersController.GetPaymentTerms)

	status := router.Group("/order/status_detail")
	// UpdateOrderStatus
	status.Get("/:database/:id/:user_id", ordersController.UpdateOrderStatus)
	// DeleteStatusDetail
	status.Delete("/:database/:id", ordersController.DeleteStatusDetail)

	client := router.Group("/order/client_address")
	// Get Client Address
	client.Get("/:database/:id", ordersController.GetAddressByClientList)

	email := router.Group("/order/email")
	// send email
	email.Get("/:database/:id", ordersController.SendInvoice)

	date := router.Group("/order_date")
	// update date
	date.Put("/:database/:id", ordersController.UpdateDateOrder)

	export := router.Group("/order_export")
	// export orders
	export.Get("/:database", ordersController.ExportOrders)

	schedule := router.Group("/order_schedule")
	// schedule orders
	schedule.Get("/:database", ordersController.ScheduleBuilder)

	sec := router.Group("secondary_colors")

	sec.Get("/:database", ordersController.SecondaryColors)

	order := router.Group("/order_info_create")
	// order info create
	order.Get("/:database", ordersController.InfoCreateOrder)

}
