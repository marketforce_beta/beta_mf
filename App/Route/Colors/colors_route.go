package Colors

import (
	"github.com/gofiber/fiber/v2"
	colorsController "gitlab.com/victorrb1015/beta_mf/App/Controller/Colors"
)

func SetupColorsRoute(router fiber.Router) {
	colors := router.Group("/colors") // Colors
	//Read All Colors
	colors.Get("/:database", colorsController.GetColors)
	//Create Color
	colors.Post("/:database", colorsController.CreateColors)
	//Read Color
	colors.Get("/:database/:id", colorsController.GetColor)
	//Update Color
	colors.Put("/:database/:id", colorsController.UpdateColor)
	//Delete Color
	colors.Delete("/:database/:id", colorsController.DeleteColor)
}
