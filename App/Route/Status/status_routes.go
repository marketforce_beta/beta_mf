package Status

import (
	"github.com/gofiber/fiber/v2"
	statusController "gitlab.com/victorrb1015/beta_mf/App/Controller/Status"
)

func SetupStatusRoutes(router fiber.Router) {
	status := router.Group("/status")
	//Read all Status
	status.Get("/:database", statusController.GetStatus)
	//Create a new Status
	status.Post("/:database", statusController.CreateStatus)
	//Read a Status
	status.Get("/:database/:id", statusController.GetStatusById)
	//Update a Status
	status.Put("/:database/:id", statusController.UpdateStatus)
	//Delete a Status
	status.Delete("/:database/:id", statusController.DeleteStatus)
}
