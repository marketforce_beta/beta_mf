package Payment

import (
	"github.com/gofiber/fiber/v2"
	paymentController "gitlab.com/victorrb1015/beta_mf/App/Controller/Payment"
)

func SetupPaymentRoutes(router fiber.Router) {
	pay := router.Group("/payment")
	// Read all payments
	pay.Get("/:database", paymentController.GetAllPayment)
	// Create a new payment
	pay.Post("/:database", paymentController.CreatePayment)
	// Update a payment
	pay.Put("/:database", paymentController.UpdatePayment)
	// Delete a payment
	pay.Delete("/:database", paymentController.DeletePayment)
}
