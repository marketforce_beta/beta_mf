package Products

import (
	"github.com/gofiber/fiber/v2"
	productsController "gitlab.com/victorrb1015/beta_mf/App/Controller/Products"
)

func SetupProductsRoutes(router fiber.Router) {
	products := router.Group("/products")
	// Get All Products
	products.Get("/:database", productsController.GetProducts)
	// Create Product
	products.Post("/:database", productsController.CreateProduct)
	// Get Product
	products.Get("/:database/:id", productsController.GetProduct)
	// Update Product
	products.Put("/:database/:id", productsController.UpdateProduct)
	// Delete Product
	products.Delete("/:database/:id", productsController.DeleteProduct)

	cat := router.Group("/catalog")
	// Create Catalog
	cat.Post("/:database", productsController.CreateCatalogLength)
	// Get Catalog
	cat.Get("/:database/:id", productsController.GetCatalogLengthByID)
	// Delete Catalog
	cat.Delete("/:database/:id", productsController.DeleteCatalogLength)

	additional := router.Group("/additional")
	// Create Additional
	additional.Post("/:database", productsController.CreateProductAdditional)
	// Get Additional
	additional.Get("/:database/:id", productsController.GetProductAdditionalByID)
	// Update Additional
	additional.Put("/:database/:id", productsController.UpdateProductAdditional)
	// Delete Additional
	additional.Delete("/:database/:id", productsController.DeleteProductAdditional)

	// Get All Acero Products
	acero := router.Group("/acero")
	acero.Get("/", productsController.GetAceroProducts)
}
