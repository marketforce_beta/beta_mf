package Category

import (
	"github.com/gofiber/fiber/v2"
	categoryController "gitlab.com/victorrb1015/beta_mf/App/Controller/Products/Category"
)

func SetupProductCategoryRoutes(router fiber.Router) {
	proCat := router.Group("/category/product")
	// Get All Product
	proCat.Get("/:database", categoryController.GetAllCategory)
	// Get Product By ID
	proCat.Get("/:database/:id", categoryController.GetCategory)
	// Create Product
	proCat.Post("/:database", categoryController.CreateCategory)
	// Update Product
	proCat.Put("/:database/:id", categoryController.UpdateCategory)
	// Delete Product
	proCat.Delete("/:database/:id", categoryController.DeleteCategory)
}
