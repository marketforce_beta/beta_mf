package SpecialPrice

import (
	"github.com/gofiber/fiber/v2"
	specialPriceController "gitlab.com/victorrb1015/beta_mf/App/Controller/SpecialPrice"
)

func SetupSpecialPriceRoutes(router fiber.Router) {
	specialPriceRoutes := router.Group("/special_price")
	//Read all special_price
	specialPriceRoutes.Get("/:database", specialPriceController.GetSpecialPrices)
	//Create a special_price
	specialPriceRoutes.Post("/:database", specialPriceController.CreateSpecialPrice)
	//Read a special_price
	specialPriceRoutes.Get("/:database/:id", specialPriceController.GetSpecialPrice)
	//Update a special_price
	specialPriceRoutes.Put("/:database/:id", specialPriceController.UpdateSpecialPrice)
	//Delete a special_price
	specialPriceRoutes.Delete("/:database/:id", specialPriceController.DeleteSpecialPrice)

	spt := router.Group("/special_price_temp")
	// Create a special_price_temp
	spt.Post("/:database", specialPriceController.CreateSPT)
	// Get By Id a special_price_temp
	spt.Get("/:database/:id", specialPriceController.GetSptByClientID)
	// Delete a special_price_temp
	spt.Delete("/:database/:id", specialPriceController.DeleteSpt)
}
