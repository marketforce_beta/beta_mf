package Client

import (
	"github.com/gofiber/fiber/v2"
	clientController "gitlab.com/victorrb1015/beta_mf/App/Controller/Client"
)

func SetupClientsRoutes(router fiber.Router) {
	client := router.Group("/client")
	// Read all clients
	client.Get("/:database", clientController.GetAllClients)
	// Create one client
	client.Post("/:database", clientController.CreateClient)
	// Read one client
	client.Get("/:database/:id", clientController.GetClient)
	// Update one client
	client.Put("/:database/:id", clientController.UpdateClient)
	// Delete one client
	client.Delete("/:database/:id", clientController.DeleteClient)
	//Get Files
	client.Get("/:database/:id/files", clientController.GetFiles)
	//Upload Files
	client.Post("/:database/:id/files", clientController.UploadCustomFiles)
	// Get states
	s := router.Group("/states")
	s.Get("/:database", clientController.GetSates)

	x := router.Group("/client_category")
	x.Post("/:database/:id", clientController.ChangeCategory)
	
}
