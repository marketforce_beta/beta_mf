package Address

import (
	"github.com/gofiber/fiber/v2"
	addressController "gitlab.com/victorrb1015/beta_mf/App/Controller/Client/Address"
)

func SetupAddressRoutes(router fiber.Router) {
	address := router.Group("/client_address")
	// Get all addresses
	address.Get("/:database", addressController.GetAllAddress)
	// Get address by id
	address.Get("/:database/:id", addressController.GetAddressByClient)
	// Create address
	address.Post("/:database", addressController.CreateAddress)
	// Update address
	address.Put("/:database/:id", addressController.UpdateAddress)
	// Delete address
	address.Delete("/:database/:id", addressController.DeleteAddress)
}
