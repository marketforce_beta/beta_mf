package Category

import (
	"github.com/gofiber/fiber/v2"
	clientCategoryController "gitlab.com/victorrb1015/beta_mf/App/Controller/Client/Category"
)

func SetupClientCategoryRoute(router fiber.Router) {
	category := router.Group("/client/category")
	// Read Client Category
	category.Get("/:database/:id", clientCategoryController.GetClientCategory)
	// Create Client Category
	category.Post("/:database", clientCategoryController.CreateClientCategory)
	// Delete Client Category
	category.Delete("/:database/:id/:category_id", clientCategoryController.DeleteClientCategory)
}
