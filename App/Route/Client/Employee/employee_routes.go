package Employee

import (
	"github.com/gofiber/fiber/v2"
	employeeController "gitlab.com/victorrb1015/beta_mf/App/Controller/Client/Employee"
)

func SetupClientEmployee(router fiber.Router) {
	employ := router.Group("/client_employee")
	// Get all employees
	employ.Get("/:database", employeeController.GetAllEmployee)
	// Get one employee
	employ.Get("/:database/:id", employeeController.GetEmployee)
	// Create one employee
	employ.Post("/:database", employeeController.CreateEmployee)
	// Update one employee
	employ.Put("/:database/:id", employeeController.UpdateEmployee)
	// Delete one employee
	employ.Delete("/:database/:id", employeeController.DeleteEmployee)
}
