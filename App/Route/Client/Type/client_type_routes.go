package Type

import (
	"github.com/gofiber/fiber/v2"
	clientTypeController "gitlab.com/victorrb1015/beta_mf/App/Controller/Client/Type"
)

func SetupClientTypeRoutes(router fiber.Router) {
	types := router.Group("/client_types")
	// Read all client types
	types.Get("/:database", clientTypeController.GetAllClientsType)
	// Read one client type
	types.Get("/:database/:id", clientTypeController.GetClientsType)
	// Create one client type
	types.Post("/:database", clientTypeController.CreateClientsType)
	// Update one client type
	types.Put("/:database/:id", clientTypeController.UpdateClientsType)
	// Delete one client type
	types.Delete("/:database/:id", clientTypeController.DeleteClientsType)
}
