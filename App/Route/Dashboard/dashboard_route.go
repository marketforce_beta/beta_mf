package Dashboard

import (
	"github.com/gofiber/fiber/v2"
	DashboardController "gitlab.com/victorrb1015/beta_mf/App/Controller/Dashboard"
)

func SetupDashboardRoute(app fiber.Router) {
	dash := app.Group("/dashboard")
	// Get Info Dashboard
	dash.Get("/:database", DashboardController.GetInfoDashboard)
}
