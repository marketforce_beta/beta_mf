package Model

import (
	"time"

	"gorm.io/gorm"
)

type BetaColors struct {
	gorm.Model
	Name         string             `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Code         string             `gorm:"type:varchar(255);not null" db:"code" valid:"required,lte=255" json:"code"`
	OrderDetails []BetaOrderDetails `gorm:"foreignkey:ColorID"`
}

type BetaProductsCategory struct {
	gorm.Model
	Name         string         `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Position     int            `gorm:"type:int;not null" db:"position" valid:"required,lte=255" json:"position"`
	BetaProducts []BetaProducts `gorm:"foreignkey:ProductsCategoryID"`
}

type BetaProducts struct {
	gorm.Model
	ProductsCategoryID       uint                     `gorm:"type:int;not null" db:"category_id" valid:"required,lte=255" json:"category_id"`
	Name                     string                   `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Gauge                    int                      `gorm:"type:int;null" db:"gauge" valid:"required,lte=255" json:"gauge"`
	Type                     string                   `gorm:"type:varchar(255);not null" db:"type" valid:"required,lte=255" json:"type"`
	Price                    float64                  `gorm:"type:decimal(10,2);not null" db:"price" valid:"required,lte=10.2" json:"price"`
	CatLength                bool                     `gorm:"type:boolean;default:false" db:"cat_length" valid:"required" json:"cat_length"`
	Colors                   bool                     `gorm:"type:boolean;default:false" db:"colors" valid:"required" json:"colors"`
	Length                   bool                     `gorm:"type:boolean;default:false" db:"length" valid:"required" json:"length"`
	Description              string                   `gorm:"type:varchar(255);null" db:"description" valid:"required,lte=255" json:"description"`
	Weight                   float64                  `gorm:"type:decimal(10,2);null" db:"weight" valid:"required,lte=10.2" json:"weight"`
	Base                     float64                  `gorm:"type:decimal(10,2);null" db:"base" valid:"lte=10.2" json:"base"`
	Altura                   float64                  `gorm:"type:decimal(10,2);null" db:"altura" valid:"lte=10.2" json:"altura"`
	Chain                    bool                     `gorm:"type:boolean;default:false" db:"chain" json:"chain"`
	WeightString             string                   `gorm:"type:varchar(255);null" db:"weight_string" valid:"required,lte=255" json:"weight_string"`
	OrderDetails             []BetaOrderDetails       `gorm:"foreignkey:ProductID"`
	OrdersProductsAdditional []BetaProductsAdditional `gorm:"foreignkey:ProductID"`
	OrdersCatalogLength      []BetaCatalogLength      `gorm:"foreignkey:ProductID"`
}

type BetaProductsAdditional struct {
	gorm.Model
	Name      string  `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Price     float64 `gorm:"type:decimal(10,2);not null" db:"price" valid:"required" json:"price"`
	ProductID uint    `gorm:"type:int;not null" db:"products_id" valid:"required" json:"products_id"`
}

type BetaCatalogLength struct {
	gorm.Model
	ProductID uint    `gorm:"type:int;not null" db:"product_id" valid:"required" json:"product_id"`
	Length    float64 `gorm:"type:decimal(10,2);not null" db:"length" valid:"required" json:"length"`
}

type BetaStatus struct {
	gorm.Model
	Name              string                  `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Description       string                  `gorm:"type:varchar(255);not null" db:"description" valid:"required,lte=255" json:"description"`
	Position          int                     `gorm:"type:int;not null" db:"position" valid:"required,lte=10" json:"position"`
	OrderStatusDetail []BetaOrderStatusDetail `gorm:"foreignkey:StatusID"`
}

type BetaPayment struct {
	gorm.Model
	Name   string       `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Orders []BetaOrders `gorm:"foreignkey:PaymentID"`
}

type BetaPaymentTerms struct {
	gorm.Model
	Name   string       `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Days   int          `gorm:"type:int;not null" db:"days" valid:"required,lte=10" json:"days"`
	Orders []BetaOrders `gorm:"foreignkey:PaymentTermID"`
}

type BetaClientTypes struct {
	gorm.Model
	Name    string        `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Clients []BetaClients `gorm:"foreignkey:ClientTypeID"`
}
type BetaSpecialPrice struct {
	gorm.Model
	ProductID  uint    `gorm:"type:int;not null" db:"product_id" valid:"required,lte=10" json:"product_id"`
	CategoryID uint    `gorm:"type:int;not null" db:"category_id" valid:"required,lte=10" json:"category_id"`
	Price      float64 `gorm:"type:decimal(10,2);not null" db:"price" valid:"required,lte=10.2" json:"price"`
	MsLock     bool    `gorm:"type:boolean;default:false" db:"ms_lock" valid:"lte=255" json:"ms_lock"`
}

type BetaClients struct {
	gorm.Model
	ClientTypeID              uint                     `gorm:"type:int;not null" db:"client_type_id" valid:"required,lte=10" json:"client_type_id"`
	CategoryID                uint                     `gorm:"type:int;not null" db:"category_id" valid:"required,lte=10" json:"category_id"`
	Name                      string                   `gorm:"type:varchar(255);null" db:"name" valid:"lte=255" json:"name"`
	FirstName                 string                   `gorm:"type:varchar(255);null" db:"first_name" valid:"lte=255" json:"first_name"`
	LastName                  string                   `gorm:"type:varchar(255);null" db:"last_name" valid:"lte=255" json:"last_name"`
	Address                   string                   `gorm:"type:varchar(255);null" db:"address" valid:"lte=255" json:"address"`
	City                      string                   `gorm:"type:varchar(255);null" db:"city" valid:"lte=255" json:"city"`
	StateID                   uint                     `gorm:"type:int;not null" db:"state_id" valid:"required,lte=10" json:"state_id"`
	Zip                       string                   `gorm:"type:varchar(255);null" db:"zip" valid:"lte=255" json:"zip"`
	Phone                     string                   `gorm:"type:varchar(255);null" db:"phone" valid:"lte=255" json:"phone"`
	Email                     string                   `gorm:"type:varchar(255);null" db:"email" valid:"lte=255" json:"email"`
	Website                   string                   `gorm:"type:varchar(255);null" db:"website" valid:"required,lte=255" json:"website"`
	Logo                      string                   `gorm:"type:varchar(255);null" db:"logo" valid:"required,lte=255" json:"logo"`
	TaxExempt                 bool                     `gorm:"type:tinyint(1);not null" db:"tax_exempt" valid:"required,lte=1" json:"tax_exempt"`
	TaxExemptDocument         string                   `gorm:"type:varchar(255);null" db:"tax_exempt_document" valid:"required,lte=255" json:"tax_exempt_document"`
	Spt                       bool                     `gorm:"type:tinyint(1);default:false" db:"spt" valid:"required,lte=1" json:"spt"`
	Category                  bool                     `gorm:"type:tinyint(1);default:false" db:"category" valid:"required,lte=1" json:"category"`
	CatalogLength             bool                     `gorm:"type:tinyint(1);default:false" db:"catalog_length" valid:"required,lte=1" json:"catalog_length"`
	DateSptUpdated            time.Time                `gorm:"type:datetime;null" db:"date_spt_updated" valid:"lte=255" json:"date_spt_updated"`
	CategoryUpdated           time.Time                `gorm:"type:datetime;null" db:"category_updated" valid:"lte=255" json:"category_updated"`
	Latitude                  float64                  `gorm:"type:decimal(10,6);null" db:"latitude" valid:"required,lte=10.6" json:"latitude"`
	Longitude                 float64                  `gorm:"type:decimal(10,6);null" db:"longitude" valid:"required,lte=10.6" json:"longitude"`
	Notes                     string                   `gorm:"type:text;null" db:"notes" valid:"lte=255" json:"notes"`
	ClientDocument            []BetaDocument           `gorm:"foreignkey:ClientID"`
	Orders                    []BetaOrders             `gorm:"foreignkey:ClientID"`
	ClientsSpecialPricingTemp []BetaSpecialPricingTemp `gorm:"foreignkey:ClientID"`
	BetaClientEmployee        []BetaClientEmployee     `gorm:"foreignkey:ClientID"`
	BetaClientAddress         []BetaClientAddress      `gorm:"foreignkey:ClientID"`
}

type BetaClientAddress struct {
	gorm.Model
	ClientID uint   `gorm:"type:int;not null" db:"client_id" valid:"required,lte=10" json:"client_id"`
	Address  string `gorm:"type:varchar(255);not null" db:"address" valid:"required,lte=255" json:"address"`
	City     string `gorm:"type:varchar(255);not null" db:"city" valid:"required,lte=255" json:"city"`
	StateID  uint   `gorm:"type:int;not null" db:"state_id" valid:"required,lte=10" json:"state_id"`
	Zip      string `gorm:"type:varchar(255);not null" db:"zip" valid:"required,lte=255" json:"zip"`
}

type BetaClientAddressStates struct {
	gorm.Model
	Name   string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Abbrev string `gorm:"type:varchar(255);not null" db:"abbrev" valid:"required,lte=255" json:"abbrev"`
}

type BetaClientEmployee struct {
	gorm.Model
	ClientID  uint   `gorm:"type:int;not null" db:"client_id" valid:"required,lte=10" json:"client_id"`
	FirstName string `gorm:"type:varchar(255);not null" db:"first_name" valid:"required,lte=255" json:"first_name"`
	LastName  string `gorm:"type:varchar(255);not null" db:"last_name" valid:"required,lte=255" json:"last_name"`
	Email     string `gorm:"type:varchar(255);not null" db:"email" valid:"required,lte=255" json:"email"`
	Phone     string `gorm:"type:varchar(255);not null" db:"phone" valid:"required,lte=255" json:"phone"`
}

type BetaSpecialPricingTemp struct {
	gorm.Model
	ClientID  uint    `gorm:"type:int;not null" db:"client_id" valid:"required,lte=10" json:"client_id"`
	ProductID uint    `gorm:"type:int;not null" db:"product_id" valid:"required,lte=10" json:"product_id"`
	Price     float64 `gorm:"type:decimal(10,2);not null" db:"price" valid:"required,lte=10.2" json:"price"`
}

type BetaCategory struct {
	gorm.Model
	Name         string             `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	SpecialPrice []BetaSpecialPrice `gorm:"foreignkey:CategoryID"`
	Clients      []BetaClients      `gorm:"foreignkey:CategoryID"`
}

type BetaDocument struct {
	gorm.Model
	ClientID uint   `gorm:"type:int;not null" db:"client_id" valid:"required,lte=10" json:"client_id"`
	Document string `gorm:"type:varchar(255);not null" db:"document" valid:"required,lte=255" json:"document"`
}

type BetaOrders struct {
	gorm.Model
	UserID            uint                    `gorm:"type:int;not null" db:"user_id" valid:"required,lte=10" json:"user_id"`
	PaymentTermID     uint                    `gorm:"type:int;not null" db:"payment_term_id" valid:"required,lte=10" json:"payment_term_id"`
	PaymentID         uint                    `gorm:"type:int;not null" db:"payment_id" valid:"required,lte=10" json:"payment_id"`
	ClientID          uint                    `gorm:"type:int;not null" db:"client_id" valid:"required,lte=10" json:"client_id"`
	AddressID         uint                    `gorm:"type:int;default:0" db:"address_id" valid:"lte=10" json:"address_id"`
	OrderNumber       uint                    `gorm:"type:int;not null" db:"order_number" valid:"required,lte=10" json:"order_number"`
	ExpirationDate    time.Time               `gorm:"type:datetime;null" db:"expiration_date" valid:"required,lte=10" json:"expiration_date"`
	TotalWeight       float64                 `gorm:"type:decimal(10,2);not null" db:"total_weight" valid:"required,lte=10.2" json:"total_weight"`
	SubTotal          float64                 `gorm:"type:decimal(10,2);not null" db:"sub_total" valid:"required,lte=10" json:"sub_total"`
	Freight           float64                 `gorm:"type:decimal(10,2);not null" db:"freight" valid:"required,lte=10" json:"freight"`
	FreightNum        float64                 `gorm:"type:decimal(10,2);not null" db:"freight_num" valid:"required,lte=10" json:"freight_num"`
	Tax               float64                 `gorm:"type:decimal(10,2);not null" db:"tax" valid:"required,lte=10" json:"tax"`
	TaxTotal          float64                 `gorm:"type:decimal(10,2);not null" db:"tax_total" valid:"required,lte=10" json:"tax_total"`
	Total             float64                 `gorm:"type:decimal(10,2);not null" db:"total" valid:"required,lte=10.2" json:"total"`
	AmountPaid        float64                 `gorm:"type:decimal(10,2);not null" db:"amount_paid" valid:"required,lte=10.2" json:"amount_paid"`
	Amount            float64                 `gorm:"type:decimal(10,2);not null" db:"amount" valid:"required,lte=10.2" json:"amount"`
	BalanceDue        float64                 `gorm:"type:decimal(10,2);not null" db:"balance_due" valid:"required,lte=10.2" json:"balance_due"`
	OrderDate         time.Time               `gorm:"type:datetime;not null" db:"order_date" valid:"required,lte=255" json:"order_date"`
	OnHold            bool                    `gorm:"type:boolean;default:false" db:"on_hold" valid:"lte=255" json:"on_hold"`
	Collection        bool                    `gorm:"type:boolean;default:false" db:"collection" valid:"lte=255" json:"collection"`
	JobPack           bool                    `gorm:"type:boolean;default:false" db:"job_pack" valid:"lte=255" json:"job_pack"`
	Export            bool                    `gorm:"type:boolean;default:false" db:"export" valid:"lte=255" json:"export"`
	Latitude          float64                 `gorm:"type:decimal(10,2);not null" db:"latitude" valid:"required,lte=10.2" json:"latitude"`
	Longitude         float64                 `gorm:"type:decimal(10,2);not null" db:"longitude" valid:"required,lte=10.2" json:"longitude"`
	Manufactured      bool                    `gorm:"type:boolean;default:false" db:"manufactured" valid:"lte=255" json:"manufactured"`
	Note              string                  `gorm:"type:varchar(255);null" db:"note" valid:"lte=255" json:"note"`
	OrdersDocuments   []BetaOrdersDocuments   `gorm:"foreignkey:OrderID"`
	OrderDetail       []BetaOrderDetails      `gorm:"foreignkey:OrderID"`
	OrderNotes        []BetaOrdersNotes       `gorm:"foreignkey:OrderID"`
	OrderStatusDetail []BetaOrderStatusDetail `gorm:"foreignkey:OrderID"`
}

type BetaOrderDetails struct {
	gorm.Model
	OrderID      uint    `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	ProductID    uint    `gorm:"type:int;not null" db:"product_id" valid:"required,lte=10" json:"product_id"`
	Quantity     uint    `gorm:"type:int;not null" db:"quantity" valid:"required,lte=10" json:"quantity"`
	ColorID      uint    `gorm:"type:int;not null" db:"color_id" valid:"required,lte=10" json:"color_id"`
	AdditionalID uint    `gorm:"type:int;default:0" db:"additional_id" valid:"lte=10" json:"additional_id"`
	Length       float64 `gorm:"type:decimal(10,2);default:0" db:"length" valid:"lte=10.2" json:"length"`
	Inches       float64 `gorm:"type:decimal(10,2);default:0" db:"inches" valid:"lte=10.2" json:"inches"`
	Fraction     float64 `gorm:"type:decimal(10,2);default:0" db:"fraction" valid:"lte=10.2" json:"fraction"`
	TotalLength  float64 `gorm:"type:decimal(10,2);not null" db:"total_length" valid:"required,lte=10.2" json:"total_length"`
	Weight       float64 `gorm:"type:decimal(10,2);not null" db:"weight" valid:"required,lte=10.2" json:"weight"`
	Rate         float64 `gorm:"type:decimal(10,2);not null" db:"rate" valid:"required,lte=10.2" json:"rate"`
	Price        float64 `gorm:"type:decimal(10,2);not null" db:"price" valid:"required,lte=10.2" json:"price"`
}

type BetaOrdersNotes struct {
	gorm.Model
	OrderID uint   `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	Note    string `gorm:"type:varchar(255);not null" db:"note" valid:"required,lte=255" json:"note"`
	UserID  uint   `gorm:"type:int;not null" db:"user_id" valid:"required,lte=10" json:"user_id"`
}

type BetaOrdersDocuments struct {
	gorm.Model
	OrderID  uint   `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	Type     string `gorm:"type:varchar(255);not null" db:"type" valid:"required,lte=255" json:"type"`
	Document string `gorm:"type:varchar(255);not null" db:"document" valid:"required,lte=255" json:"document"`
}

type BetaOrderStatusDetail struct {
	gorm.Model
	StatusID uint      `gorm:"type:int;not null" db:"status_id" valid:"required,lte=10" json:"status_id"`
	OrderID  uint      `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	UserID   uint      `gorm:"type:int;not null" db:"user_id" valid:"required,lte=10" json:"user_id"`
	Note     string    `gorm:"type:varchar(255);null" db:"note" valid:"required,lte=255" json:"note"`
	Date     time.Time `gorm:"type:datetime;null" db:"date" valid:"required,lte=10" json:"date"`
}

type User struct {
	gorm.Model
	Name     string       `db:"name" json:"name" validate:"required,lte=255"`
	LastName string       `db:"last_name" json:"last_name"`
	Password string       `db:"password" json:"password" validate:"required,lte=255"`
	Username string       `gorm:"unique" db:"username" json:"username" validate:"required,lte=255"`
	Email    string       `db:"email" json:"email" validate:"required,lte=255"`
	Orders   []BetaOrders `gorm:"foreignkey:UserID"`
}

type BetaClientStates struct {
	gorm.Model
	Name    string        `gorm:"type:varchar(255);null" db:"name" valid:"required,lte=255" json:"name"`
	Abbrev  string        `gorm:"type:varchar(255);null" db:"abbrev" valid:"required,lte=255" json:"abbrev"`
	Clients []BetaClients `gorm:"foreignkey:StateID"`
}

type BetaSecondaryColors struct {
	gorm.Model
	Name string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Code string `gorm:"type:varchar(255);not null" db:"code" valid:"required,lte=255" json:"code"`
}
