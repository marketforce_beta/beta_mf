package Router

import (
	"github.com/gofiber/fiber/v2"
	categoriesRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Category"
	clientRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Client"
	addressClientRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Client/Address"
	clientEmployeeRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Client/Employee"
	clientTypesRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Client/Type"
	colorsRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Colors"
	DashboardRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Dashboard"
	OrderRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Orders"
	OrderDocumentsRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Orders/Documents"
	OrderNotesRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Orders/Notes"
	OrderStatus "gitlab.com/victorrb1015/beta_mf/App/Route/Orders/Status"
	paymentRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Payment"
	productsRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Products"
	categoryProductsRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Products/Category"
	specialPriceRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/SpecialPrice"
	statusRoutes "gitlab.com/victorrb1015/beta_mf/App/Route/Status"
)

func PrivateRoutes(app *fiber.App) {
	api := app.Group("/api")
	colorsRoutes.SetupColorsRoute(api)
	productsRoutes.SetupProductsRoutes(api)
	statusRoutes.SetupStatusRoutes(api)
	categoriesRoutes.SetupCategoriesRoutes(api)
	specialPriceRoutes.SetupSpecialPriceRoutes(api)
	clientTypesRoutes.SetupClientTypeRoutes(api)
	clientRoutes.SetupClientsRoutes(api)
	OrderStatus.SetupOrderStatusRoutes(api)
	OrderNotesRoutes.SetupOrderNotesRoute(api)
	paymentRoutes.SetupPaymentRoutes(api)
	OrderRoutes.SetupOrdersRoutes(api)
	categoryProductsRoutes.SetupProductCategoryRoutes(api)
	addressClientRoutes.SetupAddressRoutes(api)
	clientEmployeeRoutes.SetupClientEmployee(api)
	OrderDocumentsRoutes.SetupOrderDocumentsRoutes(api)
	DashboardRoutes.SetupDashboardRoute(api)
}
