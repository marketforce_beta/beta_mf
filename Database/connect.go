package Database

import (
	"errors"
	"fmt"
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	config "gitlab.com/victorrb1015/beta_mf/Config"
	seed "gitlab.com/victorrb1015/beta_mf/Database/Seeds"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"strconv"
)

var DbC *gorm.DB

func ConnectdbClient(DbName string) error {
	var err error
	p := config.Config("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)

	if err != nil {
		log.Println("Idiot")
	}

	// Connection URL to connect to Client Database
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.Config("DB_USER"), config.Config("DB_PASSWORD"), config.Config("DB_HOST"), port, DbName)

	DbC, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Println("Failed to connect database client")
		return err
	}

	if DbC.Migrator().HasTable(&model.BetaOrders{}) {
		fmt.Println("Database " + DbName + " Connected")
	} else {
		if err = DbC.AutoMigrate(
			&model.BetaColors{},
			&model.BetaProductsCategory{},
			&model.BetaProducts{},
			&model.BetaCatalogLength{},
			&model.BetaProductsAdditional{},
			&model.BetaCatalogLength{},
			&model.BetaStatus{},
			&model.BetaPayment{},
			&model.BetaPaymentTerms{},
			&model.BetaClientTypes{},
			&model.BetaClientStates{},
			&model.BetaClientEmployee{},
			&model.BetaClientAddressStates{},
			&model.BetaClientAddress{},
			&model.BetaCategory{},
			&model.BetaClients{},
			&model.BetaSpecialPricingTemp{},
			&model.BetaSpecialPrice{},
			&model.BetaDocument{},
			&model.User{},
			&model.BetaOrders{},
			&model.BetaOrderStatusDetail{},
			&model.BetaOrderDetails{},
			&model.BetaOrdersNotes{},
			&model.BetaOrdersDocuments{},
			&model.BetaSecondaryColors{},
		); err == nil && DbC.Migrator().HasTable(&model.BetaStatus{}) {
			if err := DbC.First(&model.BetaStatus{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				//Insert seed data
				for i, _ := range seed.Status {
					err = DbC.Debug().Model(&model.BetaStatus{}).Create(&seed.Status[i]).Error
					if err != nil {
						log.Fatalf("cannot seed Status table: %v", err)
						return err
					}
				}
				for i, _ := range seed.Payments {
					err = DbC.Debug().Model(&model.BetaPayment{}).Create(&seed.Payments[i]).Error
					if err != nil {
						log.Fatalf("cannot seed Payments table: %v", err)
						return err
					}
				}
				for i, _ := range seed.PaymentsTerms {
					err = DbC.Debug().Model(&model.BetaPaymentTerms{}).Create(&seed.PaymentsTerms[i]).Error
					if err != nil {
						log.Fatalf("cannot seed Payments table: %v", err)
						return err
					}
				}
				for i, _ := range seed.ClientTypes {
					err = DbC.Debug().Model(&model.BetaClientTypes{}).Create(&seed.ClientTypes[i]).Error
					if err != nil {
						log.Fatalf("cannot seed ClientTypes table: %v", err)
						return err
					}
				}
				for i, _ := range seed.Category {
					err = DbC.Debug().Model(&model.BetaCategory{}).Create(&seed.Category[i]).Error
					if err != nil {
						log.Fatalf("cannot seed Category table: %v", err)
						return err
					}
				}
				for i, _ := range seed.StatesB {
					err = DbC.Debug().Model(&model.BetaClientStates{}).Create(&seed.StatesB[i]).Error
					if err != nil {
						log.Fatalf("cannot seed States table: %v", err)
						return err
					}
				}
				for i, _ := range seed.Colors {
					err = DbC.Debug().Model(&model.BetaColors{}).Create(&seed.Colors[i]).Error
					if err != nil {
						log.Fatalf("cannot seed Colors table: %v", err)
						return err
					}
				}
				for i, _ := range seed.StatesC {
					err = DbC.Debug().Model(&model.BetaClientAddressStates{}).Create(&seed.StatesC[i]).Error
					if err != nil {
						log.Fatalf("cannot seed States table: %v", err)
						return err
					}
				}
				for i, _ := range seed.ProductsCategory {
					err = DbC.Debug().Model(&model.BetaProductsCategory{}).Create(&seed.ProductsCategory[i]).Error
					if err != nil {
						log.Fatalf("cannot seed ProductsCategory table: %v", err)
						return err
					}
				}
				for i, _ := range seed.Client {
					err = DbC.Debug().Model(&model.BetaClients{}).Create(&seed.Client[i]).Error
					if err != nil {
						log.Fatalf("cannot seed Client table: %v", err)
						return err
					}
				}
				for i, _ := range seed.ColorsSecondary {
					err = DbC.Debug().Model(&model.BetaSecondaryColors{}).Create(&seed.ColorsSecondary[i]).Error
					if err != nil {
						log.Fatalf("cannot seed ColorsSecondary table: %v", err)
						return err
					}
				}
			}
		}
		if err != nil {
			log.Println("failed to Migrated database client")
			return err
		}
		fmt.Println(" Database " + DbName + " Migrated ")
		return err
	}
	return err
}
