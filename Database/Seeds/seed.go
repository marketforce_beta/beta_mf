package Seeds

import (
	model "gitlab.com/victorrb1015/beta_mf/App/Model"
	"time"
)

var Status = []model.BetaStatus{
	{
		Name:        "Estimate",
		Description: "Estimate",
		Position:    1,
	},
	{
		Name:        "Order",
		Description: "Order",
		Position:    2,
	},
	{
		Name:        "In Shop",
		Description: "In Shop",
		Position:    3,
	},
	{
		Name:        "Shop Done",
		Description: "Shop Done",
		Position:    4,
	},
	{
		Name:        "Shipped",
		Description: "Shipped",
		Position:    5,
	},
	{
		Name:        "Invoice",
		Description: "Invoice",
		Position:    6,
	},
	{
		Name:        "Paid",
		Description: "Paid",
		Position:    7,
	},
}

var Payments = []model.BetaPayment{
	{
		Name: "Cash",
	},
	{
		Name: "Credit Card",
	},
	{
		Name: "Debit Card",
	},
	{
		Name: "Check",
	},
}

var PaymentsTerms = []model.BetaPaymentTerms{
	{
		Name: "15 Days",
		Days: 15,
	},
	{
		Name: "30 Days",
		Days: 30,
	},
	{
		Name: "Immediate Payment",
		Days: 0,
	},
}

var ClientTypes = []model.BetaClientTypes{
	{
		Name: "Customer",
	},
	{
		Name: "Client",
	},
}

var Category = []model.BetaCategory{
	{
		Name: "Walk In",
	},
	{
		Name: "Contractor",
	},
	{
		Name: "Company",
	},
	{
		Name: "Truck Load",
	},
}

var StatesB = []model.BetaClientStates{
	{
		Name:   "Alabama",
		Abbrev: "AL",
	},
	{
		Name:   "Alaska",
		Abbrev: "AK",
	},
	{
		Name:   "Arizona",
		Abbrev: "AZ",
	},
	{
		Name:   "Arkansas",
		Abbrev: "AR",
	},
	{
		Name:   "California",
		Abbrev: "CA",
	},
	{
		Name:   "Colorado",
		Abbrev: "CO",
	}, {
		Name:   "Connecticut",
		Abbrev: "CT",
	}, {
		Name:   "Delaware",
		Abbrev: "DE",
	}, {
		Name:   "District of Columbia",
		Abbrev: "DC",
	}, {
		Name:   "Florida",
		Abbrev: "FL",
	}, {
		Name:   "Georgia",
		Abbrev: "GA",
	},
	{
		Name:   "Hawaii",
		Abbrev: "HI",
	},
	{
		Name:   "Idaho",
		Abbrev: "ID",
	},
	{
		Name:   "Illinois",
		Abbrev: "IL",
	},
	{
		Name:   "Indiana",
		Abbrev: "IN",
	},
	{
		Name:   "Iowa",
		Abbrev: "IA",
	}, {
		Name:   "Kansas",
		Abbrev: "KS",
	}, {
		Name:   "Kentucky",
		Abbrev: "KY",
	}, {
		Name:   "Louisiana",
		Abbrev: "LA",
	}, {
		Name:   "Maine",
		Abbrev: "ME",
	}, {
		Name:   "Maryland",
		Abbrev: "MD",
	},
	{
		Name:   "Massachusetts",
		Abbrev: "MA",
	},
	{
		Name:   "Michigan",
		Abbrev: "MI",
	},
	{
		Name:   "Minnesota",
		Abbrev: "MN",
	},
	{
		Name:   "Mississippi",
		Abbrev: "MS",
	},
	{
		Name:   "Missouri",
		Abbrev: "MO",
	},
	{
		Name:   "Montana",
		Abbrev: "MT",
	},
	{
		Name:   "Nebraska",
		Abbrev: "NE",
	},
	{
		Name:   "Nevada",
		Abbrev: "NV",
	},
	{
		Name:   "New Hampshire",
		Abbrev: "NH",
	},
	{
		Name:   "New Jersey",
		Abbrev: "NJ",
	},
	{
		Name:   "New Mexico",
		Abbrev: "NM",
	},
	{
		Name:   "New York",
		Abbrev: "NY",
	},
	{
		Name:   "North Carolina",
		Abbrev: "NC",
	},
	{
		Name:   "North Dakota",
		Abbrev: "ND",
	},
	{
		Name:   "Ohio",
		Abbrev: "OH",
	},
	{
		Name:   "Oklahoma",
		Abbrev: "OK",
	},
	{
		Name:   "Oregon",
		Abbrev: "OR",
	},
	{
		Name:   "Pennsylvania",
		Abbrev: "PA",
	},
	{
		Name:   "Rhode Island",
		Abbrev: "RI",
	},
	{
		Name:   "South Carolina",
		Abbrev: "SC",
	},
	{
		Name:   "South Dakota",
		Abbrev: "SD",
	},
	{
		Name:   "Tennessee",
		Abbrev: "TN",
	},
	{
		Name:   "Texas",
		Abbrev: "TX",
	},
	{
		Name:   "Utah",
		Abbrev: "UT",
	},
	{
		Name:   "Vermont",
		Abbrev: "VT",
	},
	{
		Name:   "Virginia",
		Abbrev: "VA",
	},
	{
		Name:   "Washington",
		Abbrev: "WA",
	},
	{
		Name:   "West Virginia",
		Abbrev: "WV",
	},
	{
		Name:   "Wisconsin",
		Abbrev: "WI",
	},
	{
		Name:   "Wyoming",
		Abbrev: "WY",
	},
}

var StatesC = []model.BetaClientAddressStates{
	{
		Name:   "Alabama",
		Abbrev: "AL",
	},
	{
		Name:   "Alaska",
		Abbrev: "AK",
	},
	{
		Name:   "Arizona",
		Abbrev: "AZ",
	},
	{
		Name:   "Arkansas",
		Abbrev: "AR",
	},
	{
		Name:   "California",
		Abbrev: "CA",
	},
	{
		Name:   "Colorado",
		Abbrev: "CO",
	}, {
		Name:   "Connecticut",
		Abbrev: "CT",
	}, {
		Name:   "Delaware",
		Abbrev: "DE",
	}, {
		Name:   "District of Columbia",
		Abbrev: "DC",
	}, {
		Name:   "Florida",
		Abbrev: "FL",
	}, {
		Name:   "Georgia",
		Abbrev: "GA",
	},
	{
		Name:   "Hawaii",
		Abbrev: "HI",
	},
	{
		Name:   "Idaho",
		Abbrev: "ID",
	},
	{
		Name:   "Illinois",
		Abbrev: "IL",
	},
	{
		Name:   "Indiana",
		Abbrev: "IN",
	},
	{
		Name:   "Iowa",
		Abbrev: "IA",
	}, {
		Name:   "Kansas",
		Abbrev: "KS",
	}, {
		Name:   "Kentucky",
		Abbrev: "KY",
	}, {
		Name:   "Louisiana",
		Abbrev: "LA",
	}, {
		Name:   "Maine",
		Abbrev: "ME",
	}, {
		Name:   "Maryland",
		Abbrev: "MD",
	},
	{
		Name:   "Massachusetts",
		Abbrev: "MA",
	},
	{
		Name:   "Michigan",
		Abbrev: "MI",
	},
	{
		Name:   "Minnesota",
		Abbrev: "MN",
	},
	{
		Name:   "Mississippi",
		Abbrev: "MS",
	},
	{
		Name:   "Missouri",
		Abbrev: "MO",
	},
	{
		Name:   "Montana",
		Abbrev: "MT",
	},
	{
		Name:   "Nebraska",
		Abbrev: "NE",
	},
	{
		Name:   "Nevada",
		Abbrev: "NV",
	},
	{
		Name:   "New Hampshire",
		Abbrev: "NH",
	},
	{
		Name:   "New Jersey",
		Abbrev: "NJ",
	},
	{
		Name:   "New Mexico",
		Abbrev: "NM",
	},
	{
		Name:   "New York",
		Abbrev: "NY",
	},
	{
		Name:   "North Carolina",
		Abbrev: "NC",
	},
	{
		Name:   "North Dakota",
		Abbrev: "ND",
	},
	{
		Name:   "Ohio",
		Abbrev: "OH",
	},
	{
		Name:   "Oklahoma",
		Abbrev: "OK",
	},
	{
		Name:   "Oregon",
		Abbrev: "OR",
	},
	{
		Name:   "Pennsylvania",
		Abbrev: "PA",
	},
	{
		Name:   "Rhode Island",
		Abbrev: "RI",
	},
	{
		Name:   "South Carolina",
		Abbrev: "SC",
	},
	{
		Name:   "South Dakota",
		Abbrev: "SD",
	},
	{
		Name:   "Tennessee",
		Abbrev: "TN",
	},
	{
		Name:   "Texas",
		Abbrev: "TX",
	},
	{
		Name:   "Utah",
		Abbrev: "UT",
	},
	{
		Name:   "Vermont",
		Abbrev: "VT",
	},
	{
		Name:   "Virginia",
		Abbrev: "VA",
	},
	{
		Name:   "Washington",
		Abbrev: "WA",
	},
	{
		Name:   "West Virginia",
		Abbrev: "WV",
	},
	{
		Name:   "Wisconsin",
		Abbrev: "WI",
	},
	{
		Name:   "Wyoming",
		Abbrev: "WY",
	},
}

var Colors = []model.BetaColors{
	{
		Name: "N/A",
		Code: "N/A",
	},
	{
		Name: "White",
		Code: "#fff",
	},
	{
		Name: "Forest Green",
		Code: "#fff",
	},
	{
		Name: "Hawaiian Blue",
		Code: "#fff",
	},
	{
		Name: "Zinc Gray",
		Code: "#fff",
	},
	{
		Name: "Black",
		Code: "#fff",
	},
	{
		Name: "Barn Red",
		Code: "#fff",
	},
	{
		Name: "Light Stone",
		Code: "#fff",
	},
	{
		Name: "Pebble Beige",
		Code: "#fff",
	},
	{
		Name: "Pewter Gray",
		Code: "#fff",
	},
	{
		Name: "Brown",
		Code: "#fff",
	},
	{
		Name: "Mocha Tan",
		Code: "#fff",
	},
	{
		Name: "Taupe",
		Code: "#fff",
	},
	{
		Name: "Burgundy",
		Code: "#fff",
	},
	{
		Name: "Cardinal Red",
		Code: "#fff",
	},
	{
		Name: "Galvalume",
		Code: "#fff",
	},
}

var ProductsCategory = []model.BetaProductsCategory{
	{
		Name:     "Trim",
		Position: 1,
	}, {
		Name:     "Tubing",
		Position: 2,
	}, {
		Name:     "Sheet Metal",
		Position: 3,
	}, {
		Name:     "Roll Up Door",
		Position: 4,
	}, {
		Name:     "Products",
		Position: 5,
	},
}

var Client = []model.BetaClients{
	{
		Name:            "Walking Customer",
		FirstName:       "Walking",
		LastName:        "Customer",
		StateID:         1,
		CategoryID:      1,
		ClientTypeID:    1,
		DateSptUpdated:  time.Now(),
		CategoryUpdated: time.Now(),
	},
}

var ColorsSecondary = []model.BetaSecondaryColors{
	{
		Name: "N/A",
		Code: "N/A",
	},
	{
		Name: "Forest Green",
		Code: "#fff",
	},
	{
		Name: "Black",
		Code: "#fff",
	},
	{
		Name: "Barn Red",
		Code: "#fff",
	},
	{
		Name: "Light Stone",
		Code: "#fff",
	},
	{
		Name: "Pebble Beige",
		Code: "#fff",
	},
	{
		Name: "Pewter Gray",
		Code: "#fff",
	},
	{
		Name: "Brown",
		Code: "#fff",
	},
}
